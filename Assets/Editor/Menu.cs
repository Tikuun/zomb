﻿using UnityEditor;
using UnityEngine;

    public class Menu : MonoBehaviour
    {
        [MenuItem("DEV/Time/TimeScale 0.1")]
        private static void TimeScale01()
        {
            Time.timeScale = 0.1f;
        }
        
        [MenuItem("DEV/Time/TimeScale 1")]
        private static void TimeScale1()
        {
            Time.timeScale = 1f;
        }
        
        [MenuItem("DEV/Time/TimeScale 2")]
        private static void TimeScale2()
        {
            Time.timeScale = 2f;
        }
    }

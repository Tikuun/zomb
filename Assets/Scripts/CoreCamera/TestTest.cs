using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class TestTest : MonoBehaviour
{
    private const float _radius = 0.5f;

    public Transform _circle;
    void OnDrawGizmos()
    {
        var inputX = Input.GetAxis("Horizontal");
        var inputZ = Input.GetAxis("Vertical");

        var offsetX = Vector3.right * inputX;
        var offsetZ = Vector3.forward * inputZ;

        var v = offsetX + offsetZ;
      
        
        Gizmos.DrawSphere(transform.position + v, 0.2f);




    }
}

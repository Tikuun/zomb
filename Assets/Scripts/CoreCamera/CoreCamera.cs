using System;
using Battle;
using DG.Tweening;
using UnityEngine;

namespace CoreCamera
{
    public class CoreCamera : MonoBehaviour
    {
        [SerializeField] private float _moveSpeed = 10f;
        [SerializeField] private AnimationCurve _zoomBySoldiersCount;
        [SerializeField] private Transform _camera;

        private Func<Vector3> _getTargetPos;
        
        public void OnEnterBase()
        {
            SetTargetTransform(Core.Instance.Player.VisualModel);
            SetSoldiersCount(1);
        }

        public void SetTargetTransform(Transform target)
        {
            _getTargetPos = () => target.position;
        }

        public void SetFuncGetPos(Func<Vector3> func)
        {
            _getTargetPos = func;
        }

        public void SetSoldiersCount(int count)
        {
            var zoom = _zoomBySoldiersCount.Evaluate(count);
            _camera.DOKill();
            _camera.transform.DOKill();

            _camera.DOLocalMove(new Vector3(0f, zoom, -zoom), 0.3f);
        }

        private void LateUpdate()
        {
            transform.position = Vector3.Lerp(transform.position, _getTargetPos(), Time.deltaTime * _moveSpeed);
        }

       
    }
}

﻿using System;
using UnityEngine;

namespace Utils
{
    public class TimerExecuter 
    {
        private readonly Action _action;
        private readonly float _timer;
        
        private float _checkTime;

        public TimerExecuter(Action action, float timer )
        {
            _action = action;
            _timer = timer;
            SetCooldown();
        }

        public void OnUpdate()
        {
            if (Time.time < _checkTime) return;

            SetCooldown();

            _action?.Invoke();
        }

        private void SetCooldown()
        {
            _checkTime = Time.time + _timer;
        }
    }
}
﻿
namespace Common.Random
{
    public interface IRandom
    {
        uint GetSeed();
        void SetSeed(uint val);

        uint GetCycle();
        void Dispose();
        void Reset();
        double NextDouble();
        int NextRange(int min, int max);
        float NextRange(float min, float max);
    }
}
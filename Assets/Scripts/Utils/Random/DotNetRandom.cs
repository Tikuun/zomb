﻿using System;
using UnityEngine;

namespace Common.Random
{
    public class DotNetRandom : IRandom
    {
        private System.Random _generator;
        private int _seed;
        private uint _cycle;

        public DotNetRandom(int seed = 0)
        {
            this._generator = new System.Random((int) seed);
            this._seed = (int) seed;
        }

        public void SetSeed(uint seed)
        {
            this._seed = (int) seed;
            this._generator = new System.Random(_seed);
        }

        public uint GetSeed()
        {
            return (uint) this._seed;
        }

        public uint GetCycle()
        {
            return this._cycle;
        }

        public void Dispose()
        {

        }

        public void Reset()
        {
            this._generator = new System.Random(this._seed);
            _cycle = 0;
        }

        public double NextDouble()
        {
            var val = _generator.NextDouble();
            _cycle++;
            return val;
        }

        public int NextRange(int min, int max)
        {
            var val = _generator.Next(min, max);

            _cycle++;

            return val;
        }

        public float NextRange(float min, float max)
        {
            if (max < min)
            {
                Debug.LogError("Random range float min > max");
                return max;
            }
        
            var dist = Math.Abs(max - min);
            var val01 = (float)NextDouble();

            var res = dist * val01 + min;

            return res;
        }
    }
}
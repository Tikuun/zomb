﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Random;
using UnityEngine;
using Random = System.Random;

namespace Utils
{
    public static class RandomUtils
    {
        public static T GetRandomWithWeight<T>(IRandom random, Dictionary<T, float> pairs)
        {
            float min = 0;
            var weights = new Dictionary<T, WeightRange>();
            foreach (var pair in pairs)
            {
                var max = min + pair.Value;
                weights.Add(pair.Key, new WeightRange(min, max) );

                min = max;
            }

            var randomVal = (float)random.NextDouble() * min;

            foreach (var w in weights)
            {
                if (w.Value.Contains(randomVal))
                    return w.Key;
            }

            var t = weights.Select(w => w.Value.ToString());
            var text = t.Aggregate("", (current, v) => current + v);
            throw new Exception("Can't get random with weight: " + randomVal + " Pairs: " + text);
        }

        public static bool GetBoolRandom(IRandom random, float chance)
        {
            var val = random.NextDouble();
            return chance > val;
        }

        //public static void Swap<T>(T x, T y) where T : class
        //{
        //    var z = x;
        //    x = y;
        //    y = z;
        //}

        public static List<int> GetIndexesOf<T>(this IList<T> list, T value) where T : class
        {
            var indexes = new List<int>();
            for (var index = 0; index < list.Count; index++)
            {
                var e = list[index];

                if (e == value)
                {
                    indexes.Add(index);
                }
            }

            return indexes;
        }

        public static IList<T> Shuffle<T>(this IList<T> list, IRandom rng)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.NextRange(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }
        

        public static T RandomElementUsing<T>(this IEnumerable<T> enumerable, IRandom rand) 
        {
            var count = enumerable.Count();
            if (count == 0) return default(T);
            
            int index = rand.NextRange(0, count);
            return enumerable.ElementAt(index);
        }
        

        //public static T RandomOfCondition<T>(this IList<T> list, Random rnd, Func<T, bool> cond) where T : class 
        //{
        //    var valid = new List<T>();
        //    foreach (var e in list)
        //    {
        //        if(cond(e))
        //            valid.Add(e);
        //    }

        //    if (valid.Count == 0)
        //        return null;

        //    valid.Shuffle(rnd);

        //    return valid[0];
        //}

        public static float RandomFromRange(IRandom random, float min, float max)
        {
            if (min > max)
            {
                Debug.LogError("Random From Range: invalid arguments: min > max! Max: " + max + " Min: " + min);
                return 0;
            }
            var rndValNormalized = random.NextDouble();
            var delta = max - min;

            var randomDelta = rndValNormalized * delta;

            return (float) (min + randomDelta);
        }

        public static int RandomFromRange(IRandom random, int min, int max)
        {
            var rand = RandomFromRange(random, (float) min, (float) max);
            return (int)Math.Floor(rand);
        }

        public static T GetRandomElementFromList<T>(IRandom random, List<T> collection)
        {
            int index = random.NextRange(0, collection.Count);
            return collection[index];
        }
    
        public static T GetRandomElementFromList<T>(this List<T> collection, IRandom random)
        {
            return GetRandomElementFromList<T>(random, collection);
        }
    
        
        public static T RandomElementUsing<T>(this IEnumerable<T> enumerable, Random rand)
        {
            int index = rand.Next(0, enumerable.Count());
            return enumerable.ElementAt(index);
        }
    }


    public class WeightRange
    {
        public readonly float Min;
        public readonly float Max;

        public WeightRange(float min, float max)
        {
            this.Min = min;
            this.Max = max;
        }

        public bool Contains(float val)
        {
            return val >= Min && val <= Max;
        }

        public new string ToString()
        {
            return "  [" + Min + ":" + Max + "]  ";
        }
    }
}
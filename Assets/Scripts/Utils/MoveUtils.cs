﻿using Battle;
using Characters;
using Characters.FSM;
using Characters.FSM.Soldier;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

namespace Utils
{
	public static class MoveUtils
	{
		public static async UniTask Move(this Transform me, Transform target, float duration)
		{
			var timeStart = Time.time;
			var startPos = me.position;

			await Update();
			
			void SetPosition()
			{
				me.position = Vector3.Lerp(startPos, target.position, Progress());
			}
			float Progress()
			{
				return (Time.time - timeStart) / duration;
			}

			async UniTask Update()
			{
				while (Progress()<1f)
				{
					SetPosition();

					await UniTask.Yield(PlayerLoopTiming.Update);
				}
			}
		}

		/*public static Vector3[] CalculatePath(Vector3 a, Vector3 b)
		{
			a.y = b.y = 0f;
			
			var path = new NavMeshPath();

				NavMesh.CalculatePath(a, b, NavMesh.AllAreas,  path);

			/*
			bool RandomPoint(Vector3 center, float range, out Vector3 result)
			{
				for (int i = 0; i < 30; i++)
				{
					Vector3 randomPoint = center + Random.insideUnitSphere * range;
					NavMeshHit hit;
					if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
					{
						result = hit.position;
						return true;
					}
				}
				result = Vector3.zero;
				return false;
			}
			#1#
			
			foreach (var c in path.corners)
			{
				var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				GameObject.Destroy(go.GetComponent<Collider>());
				go.transform.localScale = Vector3.one * 0.2f;
				go.transform.position = c;
                    
				GameObject.Destroy(go, 3f);
			}

			
			return path.corners;
		}*/
		
		/*public static bool IsInControlZone(CharacterControllerBase character)
		{
			var targetP = Core.Instance.ControlZone.transform.position;
			Vector3 dir = targetP - character.transform.position;

			var distanceOk =  Core.Instance.ControlZone.Radius;
			return dir.magnitude < distanceOk;
		}*/

		public static bool CanSeeByNavMesh(Vector3 a, Vector3 b)
		{
			NavMeshHit hit;
			var hitted = NavMesh.Raycast(a, b, out hit, NavMesh.AllAreas);

			return !hitted;
		}

		public static float SqrDistance(Vector3 a, Vector3 b)
		{
			return (a - b).sqrMagnitude;
		}
	}
}

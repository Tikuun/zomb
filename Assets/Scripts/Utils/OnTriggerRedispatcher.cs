﻿using System;
using UnityEngine;

namespace Utils
{
    public class OnTriggerRedispatcher : MonoBehaviour
    {
        private Action<Collider> _OnEnterAction;
        private Action<Collider> _OnExitAction;
        public void Initialize(Action<Collider> onEnter)
        {
            _OnEnterAction = onEnter;
        }
        private void OnTriggerEnter(Collider other)
        {
            _OnEnterAction?.Invoke(other);
        }
    }
}
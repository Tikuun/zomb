﻿using Battle;
using Characters;
using Characters.FSM;
using Characters.FSM.Soldier;
using Characters.FSM.Soldier.Battle;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

namespace Utils
{
	public static class SoldierHelper
	{
	
		public static StateBase CalculateNextState(CharacterControllerBase character)
		{
			if (!character.IsInCZ)
			{
				if (MoveUtils.CanSeeByNavMesh(character.Position,
					Core.Instance.ControlZone.transform.position))
				{
					return new SoldierMoveStraightToControlZoneState(character);
				}
				else
				{
					return new SoldierMoveByPathToControlZoneState(character);
				}
				
			}

			var shootState = ShootHelper.GetShootStateIfCanShoot(character);
			if (shootState != null)
			{
				return shootState;
			}
			
			return new SoldierIdleOnBattleState(character);
			
		}
	
	}
}

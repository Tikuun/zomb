﻿using System;
using System.Collections.Generic;

namespace Utils
{
	public static class CollectionUtils
	{
		public static void ForEach<T>(this IEnumerable<T> coll, Action<T> action)
		{
			foreach (var e in coll)
			{
				action(e);
			}
		}
		
		public static Queue<T> ToQueue<T>(this IEnumerable<T> coll)
		{
			var q = new Queue<T>();
			
			foreach (var e in coll)
			{
				q.Enqueue(e);
			}

			return q;
		}
	}
}

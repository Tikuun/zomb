﻿using System.Linq;
using Battle;
using Characters;
using Characters.FSM;
using Characters.FSM.Zombie;
using Common.Random;


namespace Utils
{
	public static class ZombieHelper
	{
		public static CharacterControllerBase FindNearestTarget(CharacterControllerBase zombie, int randomCount)
		{
			CharacterControllerBase target =
				Core.Instance.Soldiers.OrderBy(s => MoveUtils.SqrDistance(s.transform.position, zombie.transform.position))
					.Take(randomCount).ToList().Shuffle(new DotNetRandom(zombie.GetHashCode())).FirstOrDefault();

			if (target == null)
			{
				target = Core.Instance.Player;
			}

			return target;
		}

		public static StateBase CalculateNextState(CharacterControllerBase zombie)
		{
			if (Core.Instance.State == CoreState.Battle)
			{
				return new ZombieRunPathState(zombie);
			}
			
			var next = new ZombieReturnToSpawnState(zombie);

			var pauseDuration = UnityEngine.Random.Range(
				Core.Instance.Constants.ZombieSettings.MinPauseWhenNoTarget,
				Core.Instance.Constants.ZombieSettings.MaxPauseWhenNoTarget);
			
			var wait = new DoNothingState(zombie, pauseDuration, next);
			return wait;
		}
	}
}

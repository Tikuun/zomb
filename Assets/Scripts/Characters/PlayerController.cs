using Battle;
using Characters.FSM;
using Characters.FSM.Player.Base;
using Characters.FSM.Soldier;
using Characters.FSM.Soldier.Battle;
using Entities;
using UnityEngine;

namespace Characters
{
    public class PlayerController : CharacterControllerBase
    {
	    [SerializeField]private PlayerGoldStack _stack;
	    public PlayerGoldStack Stack => _stack;
	    protected override int HealthMax => Core.Instance.Constants.PlayerSetting.PlayerMaxHealth;
	    public override float ViewDistance =>  Core.Instance.Constants.PlayerSetting.PlayerViewDistance;
	    public override float AttackDistance => ViewDistance;

	    public override float MassWhileRun => Core.Instance.Constants.PlayerSetting.Mass_Run;
	   

	    public override void Initialize()
		{
			base.Initialize();
			Fsm.SetState(new PlayerIdleOnBaseState(this));
		}

		public bool CanSpendGold()
		{
			if (Fsm.CurrentState.CanSpendGold() == false) return false;
			if (!_stack.CanSpendGoldNow()) return false;

			return true;
		}
		
		protected override void Update()
		{
			base.Update();
			
			var speedFactor =
				Mathf.Clamp(
					Rigidbody.velocity.magnitude /
					Core.Instance.Constants.SoldiersRunSettings.DefaultSpeedForRunAnimation, 0.75f, 3f);
            
			Animator.SetFloat("SpeedFactor", speedFactor);
		}
	
		 protected override void OnTriggerEnter(Collider other)
		 {
			 base.OnTriggerEnter(other);
			 
			 if (other.GetComponent<BaseZoneTrigger>() != null)
			 {
				 Core.Instance.OnPlayerEnterBaseZone();
				 return;
			 }
			 
			 if (other.transform.root.GetComponent<ControlZone>() != null)
			 {
				 IsInCZ = true;

				 if (Core.Instance.Input.IsAnyWASD())
				 {
					 Fsm.SetState(new SoldierManualMoveState(this));
				 }

				 return;
			 }
		 }

		 public override void OnTriggerExit(Collider other)
		 {
			 base.OnTriggerExit(other);
			 
			 if (other.GetComponent<BaseZoneTrigger>() != null)
			 {
				 Core.Instance.OnPlayerEnterBattleZone();
				 return;
			 }
			 
			 if (other.transform.root.GetComponent<ControlZone>() != null)
			 {
				 IsInCZ = false;

				 Fsm.CurrentState.OnExitCZ();
			 }
			
		 }
		 
    }
}

using System.Collections.Generic;
using System.Linq;
using Battle;
using UnityEngine;
using Utils;

namespace Characters
{
	public class ZombieController : CharacterControllerBase
	{
		protected override int HealthMax => Core.Instance.Constants.ZombieSettings.HealthMax;
		public override float ViewDistance => Core.Instance.Constants.ZombieSettings.ViewDistance;
		public override float AttackDistance => Core.Instance.Constants.ZombieSettings.AttackDistance;

		public override float MassWhileRun => Core.Instance.Constants.ZombieSettings.Mass_Run;

		private readonly HashSet<CharacterControllerBase> _shootersToMe = new HashSet<CharacterControllerBase>();

		//private static int guid;
		private float _prevSpeedFactor;
		
		public override void Initialize()
		{
			base.Initialize();
			
			/*if (guid == 0)
			{
				guid = gameObject.GetInstanceID();
			}*/
			
			Fsm.SetState(ZombieHelper.CalculateNextState(this));
		}
		public override bool CanActivateCharacterSpawner()
		{
			return false;
		}

		private void OnGUI()
		{
			/*if (guid == gameObject.GetInstanceID())
			{
				GUI.color = Color.blue;
				GUILayout.Label(Rigidbody.velocity.magnitude.ToString());
			}*/
		}

		public void OnPlayerEnterBaseZone()
		{
			var next = ZombieHelper.CalculateNextState(this);
			Fsm.SetState(next);
		}

		protected override void Update()
		{
			base.Update();

			DebugCurrStateName.text = ShootersToMe.ToString();
			
			var newSpeedF =
					Rigidbody.velocity.magnitude /
					Core.Instance.Constants.ZombieSettings.RunSettings.DefaultSpeedForRunAnimation;

			newSpeedF = Mathf.Lerp(_prevSpeedFactor, newSpeedF, Time.deltaTime * 10f);
			if (newSpeedF < 0.5f) newSpeedF = 0.5f;
			
			Animator.SetFloat("SpeedFactor", newSpeedF);
			_prevSpeedFactor = newSpeedF;
		}

		public void AddShooter(CharacterControllerBase s)
		{
			_shootersToMe.Add(s);
		}

		public void RemoveShooter(CharacterControllerBase s)
		{
			_shootersToMe.Remove(s);
		}

		public int ShootersToMe => _shootersToMe.Count(s => s != null);
	}
}
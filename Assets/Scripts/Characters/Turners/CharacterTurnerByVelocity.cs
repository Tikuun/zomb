﻿/*
using UnityEngine;
using Utils;

namespace Characters.Turners
{
	public class CharacterTurnerByVelocity 
	{
		private readonly float _turnSpeed;

		private readonly Transform _target;

		float _currentYRotation = 0f;

		float fallOffAngle = 90f;

		public CharacterTurnerByVelocity(Transform target, float turnSpeed) 
		{
			_target = target;
			_turnSpeed = turnSpeed*50f;
			
			_currentYRotation = _target.localEulerAngles.y;
		}

		public void LateUpdate (Vector3 velocity)
		{
			//Project velocity onto a plane defined by the 'up' direction of the parent transform;
			velocity = Vector3.ProjectOnPlane(velocity, Vector3.up);

			float _magnitudeThreshold = 0.001f;

			//If the velocity's magnitude is smaller than the threshold, return;
			if(velocity.magnitude < _magnitudeThreshold)
				return;

			//Normalize velocity direction;
			velocity.Normalize();

			//Get current 'forward' vector;
			Vector3 _currentForward = _target.forward;

			//Calculate (signed) angle between velocity and forward direction;
			float _angleDifference = VectorMath.GetAngle(_currentForward, velocity, Vector3.up);

			//Calculate angle factor;
			float _factor = Mathf.InverseLerp(0f, fallOffAngle, Mathf.Abs(_angleDifference));

			//Calculate this frame's step;
			float _step = Mathf.Sign(_angleDifference) * _factor * Time.deltaTime * _turnSpeed;

			//Clamp step;
			if(_angleDifference < 0f && _step < _angleDifference)
				_step = _angleDifference;
			else if(_angleDifference > 0f && _step > _angleDifference)
				_step = _angleDifference;

			//Add step to current y angle;
			_currentYRotation += _step;

			//Clamp y angle;
			if(_currentYRotation > 360f)
				_currentYRotation -= 360f;
			if(_currentYRotation < -360f)
				_currentYRotation += 360f;

			//Set transform rotation using Quaternion.Euler;
			_target.localRotation = Quaternion.Euler(0f, _currentYRotation, 0f);
		}
		
		
	}
}
*/

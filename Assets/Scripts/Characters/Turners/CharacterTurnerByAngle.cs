﻿using UnityEngine;

namespace Characters.Turners
{
	public class CharacterTurnerByAngle 
	{
		private readonly float _turnSpeed;

		private readonly Transform _character;

		private readonly Quaternion _targetRotation ;

		public CharacterTurnerByAngle(Transform character, float worldAngle, float turnSpeed) 
		{
			_character = character;
			_turnSpeed = turnSpeed;

			_targetRotation = Quaternion.Euler(0, worldAngle, 0);
		}

		public void Update ()
		{
			_character.localRotation =  Quaternion.Lerp(_character.localRotation, _targetRotation, Time.deltaTime * _turnSpeed/50f);
		
		}
		
		
	}
}

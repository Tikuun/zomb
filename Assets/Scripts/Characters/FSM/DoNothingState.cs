﻿using System;
using Battle;
using Characters.FSM.Zombie;
using UnityEngine;
using UnityEngine.AI;
using Utils;

namespace Characters.FSM
{
    public sealed class DoNothingState : StateBase
    {
        private readonly StateBase _nextState;
        private readonly float _duration;
        private float _timeToExit;

        public DoNothingState(CharacterControllerBase character, float duration, StateBase nextState) : base(character)
        {
            _duration = duration;
            _nextState = nextState;
        }


        public override void OnEnter()
        {
            _timeToExit = Time.time + _duration;
        }

        public override void OnUpdate()
        {
            if (Time.time > _timeToExit)
            {
                Character.Fsm.SetState(_nextState);
            }
        }

        public override float Mass => 10f;
        public override string AnimTrigger => CharacterControllerBase.Idle;
    }
}
﻿using Battle;
using Characters.Turners;
using UnityEngine;
using Utils;

namespace Characters.FSM.Soldier.Base
{
    public sealed class SoldierInParadeLineState : StateBase
    {
        private readonly CharacterTurnerByAngle _turner;

        private readonly TimerExecuter _checkOutOfPoint;

        private readonly SoldierController _soldierController;
        
        public override float Mass => Core.Instance.Constants.SoldierSettings.Mass_Idle;
        public override string AnimTrigger => CharacterControllerBase.Idle;
        public SoldierInParadeLineState(SoldierController character) : base(character)
        {
            _soldierController = character;
            _checkOutOfPoint = new TimerExecuter(CheckIsOutOfParadePoint, 1f);
            _turner = new CharacterTurnerByAngle(character.VisualModel, 90f,
                Core.Instance.Constants.SoldierSettings.TurnSpeedOnParadeline);
        }

        public override void OnUpdate()
        {
            _checkOutOfPoint.OnUpdate();
            _turner.Update();
        }

        private void CheckIsOutOfParadePoint()
        {
            var direction = (_soldierController.GetParadelinePoint() - Character.transform.position);

            if (direction.magnitude > Core.Instance.Constants.SoldierSettings.DistanceToParadelinePoint)
            {
                Character.Fsm.SetState(new SoldierRunToParadelinePointState(_soldierController));
            }
        }
    }
}
﻿using Battle;
using Characters.Movers;
using Characters.Movers.Internal;
using UnityEngine;

namespace Characters.FSM.Soldier.Base
{
    public sealed class SoldierRunToParadelinePointState : StateBase
    {
        private readonly SoldierController _soldierController;

        private readonly CharacterMoverByPath _mover;

        public override float Mass => Core.Instance.Constants.SoldierSettings.Mass_Run;
        public override string AnimTrigger => CharacterControllerBase.Run;

        public SoldierRunToParadelinePointState(SoldierController character ) : base(character)
        {
            _soldierController = character;

            var internalMover = new CharacterMoverInternalForward(Character,
                Core.Instance.Constants.SoldierSettings.MoveSpeedToParadeline,
                Core.Instance.Constants.SoldierSettings.TurnSpeedToParadeline);
            
            _mover = new CharacterMoverByPath(Character, internalMover, _soldierController.GetParadelinePoint());
        }

        private void CheckComplete()
        {
            var dist = Vector3.Distance(_soldierController.transform.position, _soldierController.GetParadelinePoint());
            if(dist < Core.Instance.Constants.SoldierSettings.DistanceToParadelinePoint)
            {
                Character.Fsm.SetState(new SoldierInParadeLineState(_soldierController));
            }
        }


        
        public override void OnUpdate()
        {
            CheckComplete();
            //_checkComplete.OnUpdate();
            _mover.OnUpdate();
        }
        public override void OnFixedUpdate()
        {
            _mover.OnFixedUpdate();
        }

        protected  override void OnExit()
        {
            _mover.Dispose();
        }
    }
}
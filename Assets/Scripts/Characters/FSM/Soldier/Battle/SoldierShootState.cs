﻿using System;
using System.Threading;
using Battle;
using Characters.FSM.Zombie;
using Cysharp.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;
using Utils;
using Random = System.Random;

namespace Characters.FSM.Soldier.Battle
{
    public  class SoldierShootState : StateBase
    {
        private readonly ZombieController _zombie;
        private Vector3 _zombiePos;
        
        private readonly float _bulletDuration;

        public override float Mass => Character is PlayerController
            ? Core.Instance.Constants.PlayerSetting.Mass_Idle
            : Core.Instance.Constants.SoldierSettings.Mass_Idle;
        public override string AnimTrigger => null;

        private string _log = "";
        
        public SoldierShootState(CharacterControllerBase character, ZombieController target) : base(character)
        {
            _zombie = target;
            _zombiePos = _zombie.Position;
            
            var distance = Vector3.Distance(_zombie.Position, Character.Position);
            _bulletDuration = distance /Core.Instance.Constants.HitSpeed;
            if (_bulletDuration < 0.1f) _bulletDuration = 0.1f;

            _log += "ShootState started: " + Time.time + Environment.NewLine;
            _log += "Distance: " + distance+ Environment.NewLine;
            _log += "Duration: " + _bulletDuration+ Environment.NewLine;
        }

        public override void OnEnter()
        {
            _zombie.AddShooter(Character);

            Shoot()
                .AttachExternalCancellation(CancellationOnExit.Token)
                .AttachExternalCancellation(Character.GetCancellationTokenOnDestroy());
        }

       
        private async UniTask Shoot()
        {
            Character.TriggerAnimation(CharacterControllerBase.Idle, true);

            var randomPrepare = UnityEngine.Random.Range(
               Core.Instance.Constants.MinPrepareShotTime,
                   Core.Instance.Constants.MaxPrepareShotTime);
            
            //random animation preparation
            await UniTask.Delay(TimeSpan.FromSeconds(randomPrepare), cancellationToken: CancellationOnExit.Token);
            
            Character.TriggerAnimation(CharacterControllerBase.Idle, false);

            if (_zombie == null)
            {
                SetNextState();
                return;
            }

            Character.TriggerAnimation(CharacterControllerBase.Shoot, true);

            //const animation preparation
            var constDelay = TimeSpan.FromSeconds(Core.Instance.Constants.TimeOfShotFromAnimStart);
            await UniTask.Delay(constDelay, cancellationToken : CancellationOnExit.Token);

            var hitted = CheckHit();
            
            /*if (Character == null )
            {
                var nextState = SoldierHelper.CalculateNextState(Character);
                    
                _log += "While preparation EXITED: Character: " + Character + " zombie: " + _zombie + Environment.NewLine;
                _log += "nextState: " + nextState.GetType() + Environment.NewLine;

                Character.Fsm.SetState(nextState);
                return;
            }*/
            
            if (Character!= null && hitted)
            {
                var zombieTransform = _zombie != null ? _zombie.transform : null;
                Character.Gun.Shoot(_bulletDuration, zombieTransform, _zombiePos + Vector3.one);

                CreateBullet().Forget();
            }
            else
            {
               // Character.Gun.PlayMiss();
            }

            
            //animation preparation
            await UniTask.Delay(800).AttachExternalCancellation(CancellationOnExit.Token);;

            SetNextState();
        }

        private bool CheckHit()
        {
            return UnityEngine.Random.Range(0, 1f) < Core.Instance.Constants.HitProbability;
        }

        private async UniTask CreateBullet()
        {
            var bulletStartPos = Character.Position;
            
            await UniTask.Delay(TimeSpan.FromSeconds(_bulletDuration));

            if (_zombie != null)
            {
                _zombie.Rigidbody.AddForce((_zombie.Position - bulletStartPos).normalized * Core.Instance.Constants.HitPushForce, ForceMode.VelocityChange);
                //_zombie.Fsm.SetState(new ZombieHitState(_zombie));
                
                _zombie.Hit(Core.Instance.Constants.HitDamage);
            }
            else
            {
                _log += "While bullet fly EXITED: zombie: " + _zombie + Environment.NewLine;
            }
        }

        public override void OnUpdate()
        {
            if (_zombie != null)
            {
                _zombiePos = _zombie.Position;
            }
            
            Character.VisualModel.transform.forward = Vector3.Lerp(
                Character.VisualModel.transform.forward,
                _zombiePos - Character.transform.position,
                Time.deltaTime * Core.Instance.Constants.WhenShoot_TurnSpeed
            );

        
        }

        private void SetNextState()
        {
            var next = SoldierHelper.CalculateNextState(Character);
            _log += "Exit? next state = : " + next.GetType()+  Environment.NewLine;

            Character.Fsm.SetState(next);
        }

        protected  override void OnExit()
        {
//            Debug.LogWarning(_log);
            if (_zombie != null && Character != null)
            {
                _zombie.RemoveShooter(Character);
            }

            Character.TriggerAnimation(CharacterControllerBase.Shoot, false);
        }

        public override void OnDrawGizmos()
        {
            if (Character != null && _zombie != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(Character.Position+Vector3.up, _zombie.Position+Vector3.up);
            }
        }
    }
}
﻿using Battle;
using UnityEngine;
using Utils;

namespace Characters.FSM.Soldier.Battle
{
    public sealed class SoldierManualMoveState : StateBase
    {
        public override float Mass => Character is PlayerController
            ? Core.Instance.Constants.PlayerSetting.Mass_Run
            : Core.Instance.Constants.SoldierSettings.Mass_Run;

        private float _randomOffsetWiggle;
        public override string AnimTrigger => CharacterControllerBase.Run;
        
          public SoldierManualMoveState(CharacterControllerBase character) : base(character)
          {
              _randomOffsetWiggle = Random.Range(0f, 1f);
          }

          public override void OnLateUpdate()
          {
              Character.VisualModel.forward = Vector3.Lerp(Character.VisualModel.forward,
                  Core.Instance.ControlZone.MoveDir,
                  Time.deltaTime * Core.Instance.Constants.ManualMove_TurnSpeed);
          }

          public override void OnFixedUpdate()
          {
              var distToMovePoint = Vector3.Distance(Character.transform.position,
                  Core.Instance.ControlZone.GetPositionToMove());
              
              var distMax = Core.Instance.ControlZone.Radius * 2f;
              var progress = distToMovePoint / distMax;

              var speedFactorByDistToCenter = Core.Instance.Constants.SpeedInsideCZCurve.Evaluate(progress);

              var waveLength = Core.Instance.Constants.SoldiersRunSettings.RunWiggleDuration;
              var wiggleProgress = (Character.UniqueRandom01 * waveLength + Time.time % waveLength) % waveLength;
              var wiggleProgress01 = wiggleProgress / waveLength;
              var wiggleFactor = Core.Instance.Constants.SoldiersRunSettings.runSpeedWiggleCurve01.Evaluate(wiggleProgress01);
              /*Debug.LogWarning("??? waveLength " + waveLength +
                               " wiggleProgress: " + wiggleProgress +
                               " wiggleProgress01: " + wiggleProgress01 +
                               " wiggleFactor: " + wiggleFactor);*/
              
              var speedFactor = speedFactorByDistToCenter * wiggleFactor;
              var speed = Core.Instance.Constants.ManualMove_MoveSpeed * speedFactor;
              
              Character.Rigidbody.velocity = Core.Instance.ControlZone.MoveDir * speed;
          }

          public override void OnExitCZ()
          {
                base.OnExitCZ();

                Character.Fsm.SetState(SoldierHelper.CalculateNextState(Character));
          }



          protected  override void OnExit()
          {
              Character.Rigidbody.velocity = Vector3.zero;
          }
    }
}

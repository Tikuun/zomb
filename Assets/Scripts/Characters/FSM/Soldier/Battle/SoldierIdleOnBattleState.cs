﻿using Battle;
using UnityEngine;
using Utils;

namespace Characters.FSM.Soldier.Battle
{
    public sealed class SoldierIdleOnBattleState : StateBase
    {
        private readonly TimerExecuter _timerCheckExit;

        public override float Mass => Character is PlayerController
            ? Core.Instance.Constants.PlayerSetting.Mass_Idle
            : Core.Instance.Constants.SoldierSettings.Mass_Idle;
        public override string AnimTrigger => CharacterControllerBase.Idle;
        
        public SoldierIdleOnBattleState(CharacterControllerBase character) : base(character)
        {
            _timerCheckExit = new TimerExecuter(CheckExit, 0.3f);
        }

        public override void OnEnter()
        {
            CheckExit();
        }
        
        public override void OnUpdate()
        {
            _timerCheckExit.OnUpdate();

            //var dir = Core.Instance.ControlZone.LastDir.position - Core.Instance.ControlZone.transform.position;
            Character.VisualModel.forward = Vector3.Lerp(Character.VisualModel.forward, Core.Instance.ControlZone.LastDir,
                Core.Instance.Constants.WhenShoot_TurnSpeed);
        }

        private void CheckExit()
        {
            var nextState = SoldierHelper.CalculateNextState(Character);
            if (nextState.GetType() != this.GetType())
            {
                Character.Fsm.SetState(nextState);
            }
        }
       

    }
}
﻿using Battle;
using Characters.Movers.Internal;
using UnityEngine;
using Utils;

namespace Characters.FSM.Soldier.Battle
{
    public sealed class SoldierMoveStraightToControlZoneState : StateBase
    {
        private readonly CharacterMoverInternalBase _mover;
        private readonly TimerExecuter _timerCheckComplete;
        private readonly TimerExecuter _timerCheckStuck;
        private Vector3 TargetPoint => Core.Instance.ControlZone.GetPositionToMove();

        private float _timeOfEnterCZ = -1;
        private Vector3 _prevPos;

        public override float Mass => Character is PlayerController
            ? Core.Instance.Constants.PlayerSetting.Mass_Run
            : Core.Instance.Constants.SoldierSettings.Mass_Run;

        public override string AnimTrigger => CharacterControllerBase.Run;

        public SoldierMoveStraightToControlZoneState(CharacterControllerBase character) : base(character)
        {
            _timerCheckComplete = new TimerExecuter(CheckComplete, 0.5f);
            _timerCheckStuck = new TimerExecuter(CheckStuck, 0.2f);
            _mover = new CharacterMoverInternalToDir(character,
                Core.Instance.Constants.WhenSeeCZ_MoveSpeed,
                Core.Instance.Constants.WhenSeeCZ_TurnSpeed);
            _mover.SetTargetPoint(TargetPoint);

        }



        private void CheckComplete()
        {
            if (Vector3.Distance(Character.transform.position, TargetPoint) < 1f)
            {
                Character.Fsm.SetState(new SoldierIdleOnBattleState(Character));
                return;
            }

            //долго нахожусь в зоне но не достиг центра - IDLE
            if (Character.IsInCZ)
            {
                /*if (/*CheckTooLongInCz() || #1#CheckStuck())
                {
                    Character.Fsm.SetState(new SoldierIdleOnBattleState(Character));
                    return;
                }

                bool CheckTooLongInCz()
                {
                    return Time.time > _timeOfEnterCZ + 1f;
                }*/


            }

            //если пропала видимость
            if (!MoveUtils.CanSeeByNavMesh(Character.transform.position, TargetPoint))
            {
                Character.Fsm.SetState(new SoldierMoveByPathToControlZoneState(Character));
                return;
            }
        }

        void CheckStuck()
        {
            if (!Character.IsInCZ)  return;
            
            var stuck = Vector3.SqrMagnitude(_prevPos - Character.Position)
                        < Core.Instance.Constants.DistanceInCZConsiderStuck;
            _prevPos = Character.Position;

            if (!stuck)  return;

            Character.Fsm.SetState(new SoldierIdleOnBattleState(Character));
        }

        public override void OnUpdate()
        {
            _timerCheckComplete.OnUpdate();
            _timerCheckStuck.OnUpdate();
            _mover.OnUpdate();
        }

        public override void OnFixedUpdate()
        {
            _mover.SetTargetPoint(TargetPoint);
            _mover.OnFixedUpdate();
        }

        protected override void OnExit()
        {
            _mover.Dispose();
        }

        public override void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(Character.transform.position, TargetPoint);
        }

        public override void OnEnterCZ()
        {
            base.OnEnterCZ();

            _timeOfEnterCZ = Time.time;
        }

        public override void OnExitCZ()
        {
            base.OnExitCZ();

            _timeOfEnterCZ = -1f;
        }
    }
}
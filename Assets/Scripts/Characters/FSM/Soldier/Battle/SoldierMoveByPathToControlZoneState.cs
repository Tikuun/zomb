﻿using Battle;
using Characters.FSM.Soldier.Battle;
using Characters.Movers;
using Characters.Movers.Internal;
using UnityEngine;
using UnityEngine.AI;
using Utils;

namespace Characters.FSM.Soldier
{
    public sealed class SoldierMoveByPathToControlZoneState : StateBase
    {
        private CharacterMoverByPath _mover;
        private readonly TimerExecuter _timerCheckNeedRecalculatePath;
        private readonly TimerExecuter _timerCheckComplete;

        public override float Mass => Character.MassWhileRun;
        public override string AnimTrigger =>  CharacterControllerBase.Run;
        
        private Vector3 FinalTargetPoint => Core.Instance.ControlZone.transform.position;
        public SoldierMoveByPathToControlZoneState(CharacterControllerBase character) : base(character)
        {
            CreateNewMoverByPath();

            _timerCheckNeedRecalculatePath = new TimerExecuter(CheckNeedRecalculatePath, 1f);
            _timerCheckComplete = new TimerExecuter(CheckCanSeeControlZone, 1f);
        }
        
  

        private void CheckCanSeeControlZone()
        {
            if (MoveUtils.CanSeeByNavMesh(Character.transform.position, Core.Instance.ControlZone.transform.position))
            {
                Character.Fsm.SetState(new SoldierMoveStraightToControlZoneState(Character));
            }
        }

        public override void OnUpdate()
        {
            _mover.OnUpdate();
            _timerCheckNeedRecalculatePath.OnUpdate();
            _timerCheckComplete.OnUpdate();
        }

        public override void OnFixedUpdate()
        {
            _mover.OnFixedUpdate();
        }

        protected  override void OnExit()
        {
            _mover.Dispose();
        }

        public override void OnDrawGizmos()
        {
            _mover.OnDrawGizmos();
        }
        
     

        //если цель уехала больше чем на 1м - пересчитать путь
        private void CheckNeedRecalculatePath()
        {
            if (Vector3.Distance(FinalTargetPoint, _mover.TargetPoint) > 1f)
            {
                CreateNewMoverByPath();
            }
        }

        private void CreateNewMoverByPath()
        {
            var internalMover = new CharacterMoverInternalToDir(Character,
                Core.Instance.Constants.WhenDontSeeCZ_MoveSpeed,
                Core.Instance.Constants.WhenDontSeeCZ_TurnSpeed);
                
            _mover = new CharacterMoverByPath(Character, internalMover, FinalTargetPoint);
        }
    }
}
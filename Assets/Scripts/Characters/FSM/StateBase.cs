﻿using System.Threading;
using UnityEngine;

namespace Characters.FSM
{
    public abstract class StateBase
    {
        protected readonly CancellationTokenSource CancellationOnExit;
        public readonly CharacterControllerBase Character;
        protected StateBase(CharacterControllerBase characterBase)
        {
            Character = characterBase;
            CancellationOnExit = new CancellationTokenSource();
        }
        
        public virtual void OnEnter(){}
        public virtual void OnUpdate(){}
        public virtual void OnFixedUpdate(){}

        public void Exit()
        {
            CancellationOnExit.Cancel();
            OnExit();
        }
        protected virtual void OnExit(){}
        public virtual void OnLateUpdate() { }
        public virtual bool CanSwitchToOpenResourceBox()
        {
            return false;
        }

        public virtual bool  CanSpendGold()
        {
            return false;
        }

        /*public virtual void OnTriggerEnter(Collider other)
        {
            
        }*/

        public virtual void OnDrawGizmos()
        {
        }

        public virtual bool CanBeChanged()
        {
            return true;
            
        }

        public virtual void OnExitCZ()
        {
        }

        public virtual void OnEnterCZ()
        {

        }

        public abstract float Mass { get; }
        public abstract string AnimTrigger { get; }
    }
}
﻿
using UnityEngine;

namespace Characters.FSM
{
    public class FSM
    {
        public StateBase CurrentState { get; private set; }

        public void SetState(StateBase s)
        {
            if (CurrentState?.CanBeChanged()==false) return;

            
            if (CurrentState != null)
            {
                CurrentState.Exit();
                CurrentState.Character.TriggerAnimation(CurrentState.AnimTrigger, false);

            }

            //            Debug.LogWarning("State changed from: " + CurrentState + " -> " + s);

            CurrentState = s;
            CurrentState.OnEnter();

            CurrentState.Character.Rigidbody.mass = CurrentState.Mass;
            CurrentState.Character.TriggerAnimation(CurrentState.AnimTrigger, true);
            
           // s.Character.DebugCurrStateName.text = CurrentState.ToString().Replace("Characters.FSM.","" ).Replace("Soldier.","");
        }

        public void FixedUpdate()
        {
            CurrentState?.OnFixedUpdate();
        }

        public void Update()
        {
            CurrentState?.OnUpdate();
        }
        
        public void LateUpdate()
        {
            CurrentState?.OnLateUpdate();
        }
    }
    
    
}
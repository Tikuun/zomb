﻿using System;
using System.Threading;
using Battle;
using Cysharp.Threading.Tasks;
using Entities;
using UnityEngine;
using Utils;

namespace Characters.FSM.Player.Base
{
    public  class PlayerOpenResourceBoxState : StateBase
    {
        private readonly ResourceBox _box;

        public override float Mass => Core.Instance.Constants.PlayerSetting.Mass_Idle;
        public override string AnimTrigger => CharacterControllerBase.OpenLoot;
        public PlayerOpenResourceBoxState(CharacterControllerBase character, ResourceBox box) : base(character)
        {
            _box = box;
        }

        public override void OnEnter()
        {
            AnimatedHit().AttachExternalCancellation(CancellationOnExit.Token);
            
            async UniTask AnimatedHit()
            {
                await UniTask.Delay(TimeSpan.FromSeconds(Core.Instance.Constants.PlayerSetting.OpenResBox_BeforeHitTime)
                    ,cancellationToken: CancellationOnExit.Token);
                
                _box.OnHit(Core.Instance.Constants.PlayerSetting.HitDamageForeResourceBox);
                
                await UniTask.Delay(TimeSpan.FromSeconds(Core.Instance.Constants.PlayerSetting.OpenResBox_AfterHitTime)
                    ,cancellationToken: CancellationOnExit.Token);
                
                SetNextState();
            }
        }

        public override void OnUpdate()
        {
            if (Core.Instance.Input.IsAnyWASD())
            {
                Character.Fsm.SetState(new PlayerRunOnBaseState(Character));
                return;
            }

            if (_box != null && Character != null)
            {
                var dir = _box.transform.position - Character.Position;
                Character.VisualModel.forward = Vector3.Lerp(Character.VisualModel.forward, dir,
                    Time.deltaTime * Core.Instance.Constants.PlayerSetting.PlayerOnBase_TurnSpeed);
            }
            
          
        }

        private void SetNextState()
        {
            Character.Fsm.SetState(new PlayerIdleOnBaseState(Character));
        }


  
    }
}
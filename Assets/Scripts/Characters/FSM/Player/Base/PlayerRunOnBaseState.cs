﻿using Battle;
using Characters.Movers.Internal;
using UnityEngine;

namespace Characters.FSM.Player.Base
{
    public sealed class PlayerRunOnBaseState : StateBase
    {
        private readonly CharacterMoverInternalToDir _mover;

        private Vector2 _inputXY;
        
        public override float Mass => Core.Instance.Constants.PlayerSetting.Mass_Run;
        public override string AnimTrigger => CharacterControllerBase.Run;
        public PlayerRunOnBaseState(CharacterControllerBase character) : base(character)
        {
            _mover = new CharacterMoverInternalToDir(character, 
                Core.Instance.Constants.PlayerSetting.PlayerOnBase_MoveSpeed, 
                Core.Instance.Constants.PlayerSetting.PlayerOnBase_TurnSpeed);
        }

        public override void OnUpdate()
        {
            if (CheckExit())
            {
                Character.Fsm.SetState(new PlayerIdleOnBaseState(Character));
                return;
            }

            _inputXY = Core.Instance.Input.GetInputXY();
        }

        private bool CheckExit()
        {
            return Core.Instance.Input.IsComplete();
        }

        public override void OnLateUpdate()
        {
            var dir = CalculateMovementDirection();
            var point = Character.Position + dir;
            
            _mover.SetTargetPoint(point);
            
            _mover.OnUpdate();
        }
        public override void OnFixedUpdate()
        {
            _mover.OnFixedUpdate();
        }

        private Vector3 CalculateMovementDirection()
        {
            Vector3 velocity = Vector3.zero;

            velocity += Vector3.ProjectOnPlane(Core.Instance.Camera.transform.right, Vector3.up).normalized * _inputXY.x;
            velocity += Vector3.ProjectOnPlane(Core.Instance.Camera.transform.forward, Vector3.up).normalized * _inputXY.y;

            velocity.Normalize();

            return velocity;
        }

        protected override void OnExit()
        {
            _mover.Dispose();
        }
    }
}
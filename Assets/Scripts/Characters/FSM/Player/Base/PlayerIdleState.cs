﻿using Battle;

namespace Characters.FSM.Player.Base
{
    public  class PlayerIdleOnBaseState : StateBase
    {
        public PlayerIdleOnBaseState(CharacterControllerBase character) : base(character)
        {
        }

        public override void OnUpdate()
        {
            if (Core.Instance.Input.IsAnyWASD())
            {
                Character.Fsm.SetState(new PlayerRunOnBaseState(Character));
            }
        }
        public override bool CanSwitchToOpenResourceBox()
        {
            return true;
        }
        
        public override bool CanSpendGold()
        {
            return true;
        }

        public override float Mass => Core.Instance.Constants.PlayerSetting.Mass_Idle;
        public override string AnimTrigger => CharacterControllerBase.Idle;
    }
}
﻿using Battle;
using Characters.Movers;
using UnityEngine;

namespace Characters.FSM.Zombie
{
    public sealed class ZombieReturnToSpawnState : StateBase
    {
        private CharacterMoverByPath _mover;

        private readonly Vector3 _targetPos;
        public override float Mass => Core.Instance.Constants.ZombieSettings.Mass_Run;
        public override string AnimTrigger => CharacterControllerBase.Run;
        public ZombieReturnToSpawnState(CharacterControllerBase character) : base(character)
        {
            _targetPos = character.SpawnPoint;
        }

        public override void OnEnter()
        {
            var internalMover = new CharacterMoverInternalForward(Character,
                Core.Instance.Constants.ZombieSettings.MoveSpeedToSpawnPoint,
                Core.Instance.Constants.ZombieSettings.TurnSpeedToSpawnPoint);
            
            _mover = new CharacterMoverByPath(Character, internalMover, _targetPos);
        }

        public override void OnUpdate()
        {
            _mover.OnUpdate();

            if (Vector3.Distance(_targetPos, Character.Position) < 1f)
            {
                Character.Fsm.SetState( new ZombieSleepState(Character));
            }
        }

        public override void OnFixedUpdate()
        {
            _mover.OnFixedUpdate();
        }

        protected  override void OnExit()
        {
            _mover.Dispose();              
        }

        public override void OnDrawGizmos()
        {
            _mover?.OnDrawGizmos();
        }
        
    }
}
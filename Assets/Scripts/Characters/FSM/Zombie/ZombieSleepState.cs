﻿using Battle;
using Characters.Movers;
using UnityEngine;
using Utils;

namespace Characters.FSM.Zombie
{
    public sealed class ZombieSleepState : StateBase
    {
        public override float Mass => Core.Instance.Constants.ZombieSettings.Mass_Idle;
        public override string AnimTrigger => CharacterControllerBase.Idle;

        private readonly TimerExecuter _checker;
        public ZombieSleepState(CharacterControllerBase character) : base(character)
        {
            _checker = new TimerExecuter(CheckExit, 0.5f);
        }

        public override void OnUpdate()
        {
            _checker.OnUpdate();
        }

        private void CheckExit()
        {
            if (Core.Instance.State == CoreState.Base) return;

            if (HasAnyTarget())
            {
                Character.Fsm.SetState(ZombieHelper.CalculateNextState(Character));
            }
        }

        bool HasAnyTarget()
        {
            foreach (var s in Core.Instance.Soldiers)
            {
                if (SoldierIsInViewArea(s))
                    return true;
            }

            if (SoldierIsInViewArea(Core.Instance.Player))
            {
                return true;
            }

            return false;
        }
            
        bool SoldierIsInViewArea(CharacterControllerBase target)
        {
            if (Vector3.Distance(target.Position, Character.Position) > Character.ViewDistance)
                return false;
            
            if (!MoveUtils.CanSeeByNavMesh(Character.transform.position, target.Position))
            {
                return false;
            }

            return true;
        }

        
    }
}
﻿using Battle;
using Characters.FSM.Soldier;
using Characters.Movers;
using UnityEngine;
using Utils;

namespace Characters.FSM.Zombie
{
    public sealed class ZombieRunPathState : StateBase
    {
        private CharacterMoverByPath _mover;
        private readonly TimerExecuter _timerCheckExit;

        private CharacterControllerBase _targetCharacter;

        public override float Mass => Core.Instance.Constants.ZombieSettings.Mass_Run;
        public override string AnimTrigger => CharacterControllerBase.Run;

        public ZombieRunPathState(CharacterControllerBase character) : base(character)
        {
            _timerCheckExit = new TimerExecuter(IntervalCheckExit, 1f);
        }

        public override void OnEnter()
        {
            Character.Rigidbody.mass = Core.Instance.Constants.ZombieSettings.Mass_Run;

            OnUpdate();
        }

        private void IntervalCheckExit()
        {
            if (MoveUtils.CanSeeByNavMesh(Character.transform.position, _targetCharacter.transform.position))
            {
                Character.Fsm.SetState(new ZombieMoveStraightToTarget(Character));
                return;
            }

            if (Vector3.Distance(_mover.TargetPoint, _targetCharacter.transform.position) > 3f)
            {
                CalculateTarget();
                RecalculatePath();
                return;
            }
        }

        public override void OnUpdate()
        {
            if (_targetCharacter == null)
            {
                CalculateTarget();
            }

            if (_mover == null)
            {
                RecalculatePath();
            }

            RecalculateSpeed();
            
            _mover.OnUpdate();

            _timerCheckExit.OnUpdate();
        }

        public override void OnFixedUpdate()
        {
            _mover.OnFixedUpdate();
        }

        protected override void OnExit()
        {
            _mover.Dispose();
        }

        public override void OnDrawGizmos()
        {
            _mover?.OnDrawGizmos();
        }

        private void CalculateTarget()
        {
            _targetCharacter = ZombieHelper.FindNearestTarget(Character, 10);
        }

        private void RecalculatePath()
        {
            var internalMover = new CharacterMoverInternalForward(Character,
                Core.Instance.Constants.ZombieSettings.MoveSpeedWhenSeeTarget,
                Core.Instance.Constants.ZombieSettings.TurnSpeedWhenSeeTarget
            );

            _mover = new CharacterMoverByPath(Character, internalMover, _targetCharacter.transform.position);
        }
        
        private void RecalculateSpeed()
        {
            var waveLength = Core.Instance.Constants.ZombieSettings.RunSettings.RunWiggleDuration;
            var wiggleProgress = Character.UniqueRandom01 * waveLength + Time.time % waveLength;
            var wiggleProgress01 = wiggleProgress / waveLength;
            var wiggleFactor = Core.Instance.Constants.ZombieSettings.RunSettings.runSpeedWiggleCurve01.Evaluate(wiggleProgress01);
            
            _mover.SetSpeedFactor(wiggleFactor);
        }
    }
}
﻿/*using System;
using System.Threading;
using Battle;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Characters.FSM.Zombie
{
    public  class ZombieBiteState : StateBase
    {
        private readonly CharacterControllerBase _target;
        
        private readonly float _bulletDuration;

        private readonly CancellationToken _cancellation;

        public override float Mass => Core.Instance.Constants.ZombieSettings.Mass_Idle;
        public override string AnimTrigger => null;
        public ZombieBiteState(CharacterControllerBase character, CharacterControllerBase target) : base(character)
        {
            _target = target;
            _cancellation = Character.gameObject.GetCancellationTokenOnDestroy();
        }

        public override void OnEnter()
        {
            Bite()
                .AttachExternalCancellation(_cancellation)
                .AttachExternalCancellation(CancellationOnExit.Token);
        }

        private async UniTask Bite()
        {
            //random animation preparation

            Character.TriggerAnimation(CharacterControllerBase.Idle, true);

            var randomPrepare = UnityEngine.Random.Range(
                Core.Instance.Constants.ZombieSettings.MinPrepareBiteTime,
                Core.Instance.Constants.ZombieSettings.MaxPrepareBiteTime);

            await UniTask.Delay(TimeSpan.FromSeconds(randomPrepare))
                .AttachExternalCancellation(_cancellation)
                .AttachExternalCancellation(CancellationOnExit.Token);
            
            //biting
            Character.TriggerAnimation(CharacterControllerBase.Idle, false);
            Character.TriggerAnimation(CharacterControllerBase.Shoot, true);

            if (_target != null)
            {
                _target.Hit(Core.Instance.Constants.ZombieSettings.HitDamage);
            }

            //post bite const delay
            await UniTask.Delay(TimeSpan.FromSeconds(0.5f), cancellationToken: _cancellation)
                .AttachExternalCancellation(_cancellation)
                .AttachExternalCancellation(CancellationOnExit.Token);

            //var nextState = ZombieHelper.CalculateNextState(Character as ZombieController);
            Character.Fsm.SetState(new ZombieMoveStraightToTarget(Character));
        }
        

        public override void OnUpdate()
        {
            if (_target != null)
            {
                Character.VisualModel.transform.forward = Vector3.Lerp(
                    Character.VisualModel.transform.forward,
                    _target.transform.position - Character.transform.position,
                    Time.deltaTime*Core.Instance.Constants.ZombieSettings.HitDamage
                );
            }
        }

        protected  override void OnExit()
        {
            Character.TriggerAnimation(CharacterControllerBase.Shoot, false);
        }

        public override void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(Character.transform.position, 1f);
        }


    }
}*/
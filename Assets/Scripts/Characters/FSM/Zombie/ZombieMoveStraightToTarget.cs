﻿using System;
using Battle;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Utils;

namespace Characters.FSM.Zombie
{
    public sealed class ZombieMoveStraightToTarget : StateBase
    {
        private readonly TimerExecuter _timerCheckChangeState; // bite / path 
        private readonly TimerExecuter _timerChangeTarget; //  calculate new Target
        private CharacterControllerBase _target;

        private readonly CharacterMoverInternalBase _mover;

        public override float Mass => Core.Instance.Constants.ZombieSettings.Mass_Run;
        public override string AnimTrigger => CharacterControllerBase.Run;

        private bool _isAttacking;
        private float _startMoveTime;
        private bool _isMoving => _startMoveTime > 0f;
        private float _moveDuration => Time.time - _startMoveTime;
        
        private Vector3 _prevPos;

        public ZombieMoveStraightToTarget(CharacterControllerBase zombieController) : base(zombieController)
        {
            _timerCheckChangeState = new TimerExecuter(IntervaledCheckComplete, 0.5f);
            _timerChangeTarget = new TimerExecuter(RecalculateTarget, 2f);

            _mover = new CharacterMoverInternalForward(Character,
                Core.Instance.Constants.ZombieSettings.MoveSpeedWhenSeeTarget,
                Core.Instance.Constants.ZombieSettings.TurnSpeedWhenSeeTarget
            );
        }


        public override void OnEnter()
        {
            OnUpdate();
        }
        
        private void IntervaledCheckComplete()
        {
            if(!MoveUtils.CanSeeByNavMesh(Character.transform.position, _target.transform.position))
            {
                Character.Fsm.SetState(new ZombieRunPathState(Character));
                return;
            }
        }

        private async UniTask Attack()
        {
            _isAttacking = true;
            Character.TriggerAnimation("Shoot", true);

            await UniTask.Delay(TimeSpan.FromSeconds(0.2f));
            
            if (Character!= null && _target != null)
            {
                _target.Hit(Core.Instance.Constants.ZombieSettings.HitDamage);
            }

            //post bite const delay
            await UniTask.Delay(TimeSpan.FromSeconds(0.8f));

            if (Character != null)
            {
                Character.TriggerAnimation("Shoot", false);
            }
            _isAttacking = false;
        }

        public override void OnUpdate()
        {
            if (_target == null)
            {
                RecalculateTarget();
            }
            
            _timerCheckChangeState.OnUpdate();
            _timerChangeTarget.OnUpdate();

            
            var dist = Vector3.Distance(Character.transform.position, _target.transform.position);
            if (dist < Character.AttackDistance)
            {
                if (!_isAttacking )
                {
                    Attack().Forget();
                    Character.TriggerAnimation("Shoot", true);
                    // Character.Fsm.SetState(new ZombieBiteState(Character, _target));
                    return;
                }

                if (_isMoving && _moveDuration>1f)
                {
                    StopMove();
                }
            }
            else
            {
                if (!_isMoving)
                {
                    StartMove();
                }
                RecalculateSpeed();
            }
            
            _mover.OnUpdate();
            
        }
        

        private void RecalculateTarget()
        {
            int randomCount = 5;
            if (_target != null && Vector3.Distance(_target.Position, Character.Position) <
                Character.AttackDistance * 3f)
            {
                randomCount = 1;
            }
            
            _target = ZombieHelper.FindNearestTarget(Character, randomCount);
        }

        public override void OnFixedUpdate()
        {
            if (_target == null) return;

            _mover.SetTargetPoint(_target.Position);
            _mover.OnFixedUpdate();
        }

        protected  override void OnExit()
        {
            _mover.Dispose();
        }

        public override void OnDrawGizmos()
        {
            if (_target == null) return;
            
            Gizmos.color = Color.green;
            Gizmos.DrawLine(Character.Position, _target.Position);
        }

        private void RecalculateSpeed()
        {
            var waveLength = Core.Instance.Constants.ZombieSettings.RunSettings.RunWiggleDuration;
            var wiggleProgress = Character.UniqueRandom01 * waveLength + Time.time % waveLength;
            var wiggleProgress01 = wiggleProgress / waveLength;
            var wiggleFactor = Core.Instance.Constants.ZombieSettings.RunSettings.runSpeedWiggleCurve01.Evaluate(wiggleProgress01);
            
            _mover.SetSpeedFactor(wiggleFactor);
        }

        private void StartMove()
        {
            _startMoveTime = Time.time;
            
            Character.TriggerAnimation("Idle", false);
            Character.TriggerAnimation("Run", true);
            
            Character.Rigidbody.mass = Core.Instance.Constants.ZombieSettings.Mass_Run;

        }

        private void StopMove()
        {
            _startMoveTime = -1f;
            
            _mover.SetSpeedFactor(0f);
            Character.TriggerAnimation("Idle", true);
            Character.TriggerAnimation("Run", false);

            Character.Rigidbody.mass = Core.Instance.Constants.ZombieSettings.Mass_Idle;

        }
    }
}
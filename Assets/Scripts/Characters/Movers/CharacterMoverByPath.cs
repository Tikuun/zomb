using System.Collections.Generic;
using Battle;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.TextCore.Text;
using Utils;

namespace Characters.Movers
{
    public class CharacterMoverByPath
    {
	    private readonly CharacterMoverInternalBase _internalMover;
	    
	    private readonly CharacterControllerBase _character;
	    
	    public readonly Vector3 TargetPoint;

	    private Queue<Vector3> _path;
	    private Vector3 _dirToCurrCorner;
	    
	    private readonly TimerExecuter _checkStuck;
	    private Vector3 _prevPos;
	    

		public CharacterMoverByPath(CharacterControllerBase character, CharacterMoverInternalBase internalMover, Vector3 targetPoint)
		{
			_internalMover = internalMover;
			_character = character;
			TargetPoint = targetPoint;

			_checkStuck = new TimerExecuter(CheckStuck, 0.4f);
		}

		private void CheckStuck()
		{
			if (Vector3.SqrMagnitude(_prevPos - _character.Position) < 3)
			{
				_path = RecalculatePath(TargetPoint);
			}

			_prevPos = _character.Position;
		}

		public void OnUpdate()
		{
			if (!PathIsOk())
			{
				_path = RecalculatePath(TargetPoint);
			}

			if (!PathIsOk()) return;

			
			var currCorner = _path.Peek();
			currCorner.y = 0;
			
			_internalMover.SetTargetPoint(currCorner);
			_internalMover.OnUpdate();
			
			var moveDir = currCorner - _character.Position;
			var minDist = _path.Count == 1 ? 0.2f : 1f;
			if (moveDir.magnitude < minDist || Vector3.Angle(moveDir, _dirToCurrCorner) > 90f)
			{
				_path.Dequeue();
				if (_path.Count > 0)
				{
					_dirToCurrCorner = _path.Peek() - _character.Position;
				}
			}
			
			_checkStuck.OnUpdate();
		}

		public void OnFixedUpdate()
		{
			_internalMover.OnFixedUpdate();
		}	
		
		private bool PathIsOk()
		{
			if (_path == null) return false;
			if (_path.Count == 0) return false;
			return true;
		}

		private Queue<Vector3> RecalculatePath(Vector3 targetPoint, int attempts = 10)
		{
			var path = new NavMeshPath();
			NavMesh.CalculatePath(_character.transform.position,
				targetPoint,
				NavMesh.AllAreas,
				path);


			if (path.corners.Length == 0 && attempts > 0)
			{
				_character.name = _character.gameObject.GetInstanceID().ToString();
				Debug.LogError("Path not found: " + _character.name);
				NavMeshHit hit;
				var succeed = NavMesh.SamplePosition(Core.Instance.ControlZone.transform.position,
					out hit,
					5f,
					NavMesh.AllAreas);

				if (succeed)
				{
					return RecalculatePath(hit.position, attempts-1);
				}
				else
				{
					return null;
				}
			}


			return path.corners.ToQueue();
		}
		
		public void OnDrawGizmos()
		{
			if (!PathIsOk()) return;
			
			Gizmos.color = Color.red;
			var path = _path.ToArray();
            
			Gizmos.DrawLine(_character.transform.position, path[0]);

			for (int i = 0; i < path.Length; i++)
			{
				var a = path[i] + Vector3.up;
				if (path.Length > i + 1)
				{
					var b = path[i + 1]+ Vector3.up;
					Gizmos.DrawLine(a, b);
				}
			}
		}

		public void Dispose()
		{
			_internalMover.Dispose();	
		}


		public void SetSpeedFactor(float factor)
		{
			_internalMover.SetSpeedFactor(factor);
		}
    }
}

using System.Collections.Generic;
using Battle;
using UnityEngine;
using UnityEngine.AI;
using Utils;

namespace Characters
{
    public class CharacterMoverInternalForward : CharacterMoverInternalBase
    {
	    private Vector3 _currentForward;
	    private Vector3 TargetForward => TargetPoint - Character.Position;

		public CharacterMoverInternalForward(CharacterControllerBase character, float moveSpeed, float turnSpeed):base(character, moveSpeed, turnSpeed)
		{
			_currentForward = character.VisualModel.forward;
		}
		
		public override void OnUpdate()
		{
			if (Vector3.Distance(Character.Position, TargetPoint) < 0.1f)
				return;
			
			_currentForward = Vector3.Lerp(_currentForward, TargetForward, Time.deltaTime * TurnSpeed);
			Character.VisualModel.forward = _currentForward;
		}

		public override void OnFixedUpdate()
		{
			ApplyMove(_currentForward.normalized);
		}	
		
	
		
		public override void Dispose()
		{
			ApplyMove(Vector3.zero);	
		}

		private void ApplyMove(Vector3 dir)
		{
			Character.Rigidbody.velocity = dir * MoveSpeed;
		}
	
    }
}

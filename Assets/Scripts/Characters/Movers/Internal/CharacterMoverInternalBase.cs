using UnityEngine;

namespace Characters
{
	//как происходит перемещение?
	//например всегда лицом вперед, меняя угол или движение в сторону цели+поворот вторичен
	public abstract class CharacterMoverInternalBase
    {
	    protected  readonly CharacterControllerBase Character;
	    private readonly float _defaultSpeed;
	    private float _speedFactor = 1f;

	    protected float MoveSpeed => _defaultSpeed * _speedFactor;
	    protected  readonly float TurnSpeed;

	    protected Vector3 TargetPoint;

	    protected CharacterMoverInternalBase(CharacterControllerBase character, float moveSpeed, float turnSpeed)
		{
			Character = character;
			_defaultSpeed = moveSpeed;
			TurnSpeed = turnSpeed;
		}

		public void SetTargetPoint(Vector3 p)
		{
			TargetPoint = p;
		}

		public abstract void OnUpdate();

		public abstract void OnFixedUpdate();

		public abstract void Dispose();

		public void SetSpeedFactor(float factor)
		{
			_speedFactor = factor;
		}
    }
}

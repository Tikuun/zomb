using UnityEngine;

namespace Characters.Movers.Internal
{
    public class CharacterMoverInternalToDir : CharacterMoverInternalBase
    {
	 
	    public CharacterMoverInternalToDir(CharacterControllerBase character, float moveSpeed, float turnSpeed) : base(character, moveSpeed, turnSpeed)
	    {
	    }

	    public override void OnUpdate()
	    {
		    Character.VisualModel.forward = Vector3.Lerp(Character.VisualModel.forward,
			    TargetPoint - Character.Position, Time.deltaTime * TurnSpeed);
	    }

	    public override void OnFixedUpdate()
	    {
		    var dir = TargetPoint - Character.Position;
		    
		    Character.Rigidbody.velocity = dir.normalized * MoveSpeed;
	    }

	    public override void Dispose()
	    {
		    Character.Rigidbody.velocity = Vector3.zero;
	    }

	  
    }
}

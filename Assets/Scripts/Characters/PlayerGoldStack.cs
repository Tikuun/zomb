using System.Collections.Generic;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using Utils;

namespace Characters
{
    public class PlayerGoldStack : MonoBehaviour
    {
	    [SerializeField] private int _goldCountMax;
	    [SerializeField] private GameObject _goTemplate;
	    [SerializeField] private Transform _stackRoot;
	    [SerializeField] private float _offset = 0.3f;
	    [SerializeField] private float _spendCooldown = 0.1f;

	    private readonly Stack<GameObject> _stack = new Stack<GameObject>();
	    
	    private int _goldCount;
	    private int GoldCount => _goldCount;

	    private float _lastSpendTime;
	    private bool IsCooldown => _lastSpendTime + _spendCooldown > Time.time ;
	    
	    [ContextMenu("Add")]
	    public void Add()
	    {
		    if (_goldCount == _goldCountMax) return;

		    _goldCount++;

		    foreach (var go in _stack)
		    {
			    go.transform.Translate(0, _offset, 0, Space.Self);
		    }

		    var newGo =Instantiate(_goTemplate, _stackRoot.position, Quaternion.identity, _stackRoot);
		    newGo.transform.localRotation = Quaternion.identity;
		    newGo.gameObject.SetActive(true);
		    
		    _stack.Push(newGo);
	    }
	    
	    [ContextMenu("Use")]


	 

		public async Task CollectGold(GameObject gold)
		{
			await gold.transform.Move(_stackRoot.transform, 0.25f);
			Add();
		}

		public async Task SpendGold(Vector3 position)
		{
			if (_goldCount == 0) return;
	    
			_goldCount--;

			foreach (var go in _stack)
			{
				go.transform.Translate(0, -_offset, 0, Space.Self);
			}
			
			var last = _stack.Pop();
			last.transform.SetParent(null);

			_lastSpendTime = Time.time;
			await last.transform.DOMove(position, 0.15f);
			Destroy(last);
		}

		public bool CanSpendGoldNow()
		{
			return GoldCount > 0 && !IsCooldown;
		}
    }
}

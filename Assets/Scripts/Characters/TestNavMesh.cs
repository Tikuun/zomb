﻿using Battle;
using UnityEngine;
using UnityEngine.AI;

namespace Characters
{
    public class TestNavMesh
    {
        [SerializeField] private Transform _a;
        [SerializeField] private Transform _b;
        
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                var path = new NavMeshPath();
                
                NavMesh.CalculatePath(_a.position,
                    _b.position,
                    NavMesh.AllAreas,
                    path);

                foreach (var c in path.corners)
                {
                    var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    GameObject.Destroy(go.GetComponent<Collider>());
                    go.transform.localScale = Vector3.one * 0.2f;
                    go.transform.position = c;
                    
                    GameObject.Destroy(go, 5f);
                }
            }
        }
    }
}
using System;
using Battle;
using Common.Random;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Characters
{
    public abstract class CharacterControllerBase : MonoBehaviour
    {
        [SerializeField] private Transform _visualModel;

        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] public Gun Gun;

        [SerializeField] private Image _healthBar;

       
        [SerializeField] public TextMeshProUGUI DebugCurrStateName;

        [SerializeField] private Animator _animator;
        [SerializeField] private GameObject _dieFx;
        
        protected abstract int HealthMax { get; }
        public abstract float ViewDistance { get; }
        public abstract float AttackDistance { get; }
        
        private Transform MyTransform;
        public Vector3 Position => MyTransform.position;
        public Animator Animator => _animator;

        private int _health;

        public readonly FSM.FSM Fsm = new FSM.FSM();
        
        public Rigidbody Rigidbody => _rigidbody;
        public Transform VisualModel => _visualModel;

        public bool IsInCZ { get; protected set; }
        public abstract float MassWhileRun { get; }
        
        public float UniqueRandom01 { get; private set; }

        public Vector3 SpawnPoint { get; private set; }

        public virtual void Initialize()
        {
          
            MyTransform = transform;
            _health = HealthMax;
            UniqueRandom01 = (float) new DotNetRandom(gameObject.GetHashCode()).NextDouble();
            
            SpawnPoint = transform.position;

        }
        private void LateUpdate()
        {
            Fsm.LateUpdate();
        }

      
        protected virtual void Update()
        {
            Fsm.Update();


        }

        private void FixedUpdate()
        {
            Fsm.FixedUpdate();
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
     
        }


        public virtual void OnTriggerExit(Collider other)
        {
           
        }
        
     
       
        /*public bool OnEnterZone(EntityBase entity)
        {
            Fsm.CurrentState.
        }*/
        /*
         * private ActionQueue _queue;
        */

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying) return;

            Fsm.CurrentState.OnDrawGizmos();
        }

        public virtual bool CanActivateCharacterSpawner()
        {
            return true;
        }

        protected virtual void Die()
        {
            var fx = Instantiate(_dieFx.gameObject, gameObject.transform.position, Quaternion.identity);
            
            Destroy(fx, 3f);
            Destroy(gameObject);
        }

        public virtual void Hit(int val)
        {
            _health -= val;
            if (_health <= 0)
            {
                Die();
            }
            else
            {
                ActualizeHealthBar();
            }
        }

        private void ActualizeHealthBar()
        {
            _healthBar.fillAmount = _health / (float) HealthMax;
        }

        private void OnEnable()
        {
            Core.Instance.CharacterEnabled(this);
        }

        private void OnDisable()
        {
            Core.Instance.CharacterDisabled(this);
        }

        private void OnValidate()
        {
            _animator = GetComponentInChildren<Animator>();
        }
        
        public const string Idle = "Idle";
        public const string Run = "Run";
        public const string Shoot = "Shoot";
        public const string OpenLoot = "Loot";
        public void TriggerAnimation(string trigger, bool active)
        {
            if (_animator != null && trigger!= null)
            {
                if (active)
                {
                    _animator.SetTrigger(trigger);

                }
                else
                {
                    _animator.ResetTrigger(trigger);
                }
            }
        }
    }
}

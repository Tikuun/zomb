using Battle;
using Characters.CharacterInput;
using Characters.FSM;
using Characters.FSM.Soldier;
using Characters.FSM.Soldier.Base;
using Characters.FSM.Soldier.Battle;
using Entities;
using UnityEngine;

namespace Characters
{
	public class SoldierController : CharacterControllerBase
	{
		private ParadeLineLocator _spawnPoint;

		protected override int HealthMax => Core.Instance.Constants.SoldierSettings.Health;
		public override float ViewDistance => Core.Instance.Constants.SoldierSettings.ViewDistance;
		public override float AttackDistance => ViewDistance;

		public override float MassWhileRun => Core.Instance.Constants.SoldierSettings.Mass_Run;

		public override void Initialize()
		{
			base.Initialize();

			if (Core.Instance.State == CoreState.Base)
			{
				_spawnPoint = Core.Instance.ParadeLine.BookFreeSlot(this);
				Fsm.SetState(new SoldierRunToParadelinePointState(this));	
			}
			else
			{
				Fsm.SetState(new SoldierIdleOnBattleState(this));	
			}		
			
			Animator.SetFloat("CycleOffset", Random.Range(0f, 1f));
		}

		public void OnPlayerEnterBattleZone()
		{
			Fsm.SetState(new SoldierIdleOnBattleState(this));
		}

		public void OnPlayerEnterBaseZone()
		{
			Fsm.SetState(new SoldierRunToParadelinePointState(this));
		}

		protected override void Die()
		{
			base.Die();
			Core.Instance.ControlZone.OnSoldierDied(this);
		}

		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);
			 
			if (other.transform.root.GetComponent<ControlZone>() != null)
			{
				IsInCZ = true;

				Fsm.CurrentState.OnEnterCZ();

				if (Core.Instance.Input.IsAnyWASD())
				{
					Fsm.SetState(new SoldierManualMoveState(this));
				}

				return;
			}
		}

		public override void OnTriggerExit(Collider other)
		{
			base.OnTriggerExit(other);
			 
			if (other.transform.root.GetComponent<ControlZone>() != null)
			{
				IsInCZ = false;

				Fsm.CurrentState.OnExitCZ();
			}
			
		}


		public Vector3 GetParadelinePoint()
		{
			if (_spawnPoint == null) return transform.position;

			return _spawnPoint.transform.position;
		}

		protected override void Update()
		{
			base.Update();
			
			var speedFactor =
				Mathf.Clamp(
					Rigidbody.velocity.magnitude /
					Core.Instance.Constants.SoldiersRunSettings.DefaultSpeedForRunAnimation, 0.75f, 3f);
            
			Animator.SetFloat("SpeedFactor", speedFactor);
		}
	}
}

﻿using UnityEngine;

namespace Characters.CharacterInput
{
    public class JoystickInput : CharacterInputBase
    {
        private readonly Joystick _joystick;
        public JoystickInput(Joystick j)
        {
            _joystick = j;
        }
            
        public override float GetHorizontal()
        {
            return _joystick.Horizontal;
        }

        public override float GetVertical()
        {
            return _joystick.Vertical;
        }

        public override bool IsComplete()
        {
            return !IsAnyWASD();
        }

        public override bool IsAnyWASD()
        {
            return _joystick.Direction != Vector2.zero;
        }

        public override void Subscribe(IInputSubscriber subscriber)
        {
            _joystick.Subscribe(subscriber);
        }

        public override void Unsubscribe(IInputSubscriber subscriber)
        {
            _joystick.Unsubscribe(subscriber);
        }
    }
}
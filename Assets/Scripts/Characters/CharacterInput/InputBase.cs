﻿using UnityEngine;

namespace Characters.CharacterInput
{
    public abstract class CharacterInputBase
    {
        public abstract float GetHorizontal();
        public abstract float GetVertical();

        public Vector2 GetInputXY()
        {
            return new Vector2(GetHorizontal(), GetVertical());
        }

        public abstract bool IsComplete();

        public abstract bool IsAnyWASD();

        public abstract void Subscribe(IInputSubscriber subscriber);

        public abstract void Unsubscribe(IInputSubscriber subscriber);

        public virtual void Update()
        {
            
        }
    }
}
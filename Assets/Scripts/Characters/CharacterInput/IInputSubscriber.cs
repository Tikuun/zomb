﻿namespace Characters.CharacterInput
{
    public interface IInputSubscriber
    {
        void OnInputStarted();
        void OnInputFinished();
    }
}
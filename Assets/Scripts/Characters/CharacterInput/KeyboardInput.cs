﻿/*using System.Collections.Generic;
using UnityEngine;

namespace Characters.CharacterInput
{
    public class KeyboardInput : CharacterInputBase
    {
        private readonly HashSet<IInputSubscriber> _subscribers = new HashSet<IInputSubscriber>();

        private static List<KeyCode> _awsd = new List<KeyCode>()
        {
            KeyCode.A,
            KeyCode.W,
            KeyCode.S,
            KeyCode.D
        };

        public override float GetHorizontal()
        {
            return Input.GetAxis("Horizontal");

        }

        public override float GetVertical()
        {
            return Input.GetAxis("Vertical");
        }

        public override bool IsComplete()
        {
            return !IsAnyWASD();
        }

        public override bool IsAnyWASD()
        {
            foreach (var keyCode in _awsd)
            {
                if (Input.GetKey(keyCode))
                {
                    return true;
                }
            }

            return false;
        }

        public override void Subscribe(IInputSubscriber subscriber)
        {
            _subscribers.Add(subscriber);
        }

        public override void Unsubscribe(IInputSubscriber subscriber)
        {
            _subscribers.Remove(subscriber);
        }

        /*public override bool IsInputStarted()
        {
            foreach (var keyCode in _awsd)
            {
                if (Input.GetKeyDown(keyCode))
                {
                    return true;
                }
            }

            return false;
        }
        
        public override bool IsInputFinished()
        {
            return IsAnyWASDUp() && !IsAnyWASD();
        }#1#

        private bool IsAnyWASDUp()
        {
            foreach (var keyCode in _awsd)
            {
                if (Input.GetKeyUp(keyCode))
                {
                    return true;
                }
            }

            return false;
        }

        public override void Update()
        {
            foreach (var keyCode in _awsd)
            {
                if (Input.GetKeyDown(keyCode))
                {
                    DispatchInputStarted();
                }
            }

            
            bool IsAnyWASDUp()
            {
                foreach (var keyCode in _awsd)
                {
                    if (Input.GetKeyUp(keyCode))
                    {
                        return true;
                    }
                }

                return false;
            }
            
        }

        private void DispatchInputStarted()
        {
            foreach (var subscriber in _subscribers)
            {
                subscriber.OnInputStarted();
            }
        }
    }
}*/
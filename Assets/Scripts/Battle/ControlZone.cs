using System;
using System.Collections.Generic;
using Characters;
using Characters.CharacterInput;
using Characters.FSM.Soldier.Battle;
using Common.Random;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Battle
{
    public class ControlZone : MonoBehaviour, IInputSubscriber
    {
	    [SerializeField] private Rigidbody _rigidbody;
	    [SerializeField] private float _speed = 10f;
	    [SerializeField] private float _arrowRotSpeed = 20f;
	    
	    [SerializeField] private Transform _arrowRoot;
	    
	    [SerializeField] private Transform _circleRoot;
	    [SerializeField] private SphereCollider _colliderPhysical;
	    [SerializeField] private SphereCollider _colliderTrigger;
	    [SerializeField] private AnimationCurve _radiusByCharactersCount;
	    [SerializeField] private TextMeshProUGUI _count;
	    
	    [SerializeField] private AnimationCurve _funnelForceByDistanceToCenter;
	    [SerializeField] private float _maxFunnelForce = 1f;

	    public Vector3 LastDir => _arrowRoot.forward;
	    public float Speed => _speed;
	    private CharacterInputBase _input => Core.Instance.Input;

	    private Vector3 _cameraVectorX;
	    private Vector3 _cameraVectorZ;

	    private readonly List<CharacterControllerBase> _charactersInside = new List<CharacterControllerBase>();

	    public Vector3 MoveDir => _moveDir;
	    private Vector3 _moveDir;
	    public bool IsMoving => _moveDir.magnitude > 0f;
	    private float _radius;

	    public float Radius => _radius;
	    private Transform _transform;

	    public void Enable()
	    {
		    _transform = transform;
		    _cameraVectorX = Vector3.ProjectOnPlane(Core.Instance.Camera.transform.right, Vector3.up).normalized;
		    _cameraVectorZ = Vector3.ProjectOnPlane(Core.Instance.Camera.transform.forward, Vector3.up).normalized;

		    _transform.position = Core.Instance.Player.VisualModel.position;
		    _arrowRoot.forward = Core.Instance.Player.VisualModel.forward;
		    gameObject.SetActive(true);

		  //  _onTriggerEnterRedispatcher.Initialize(OnTriggerEnter);
			    
		    ActualizeRadius();

		    Core.Instance.Input.Subscribe(this);
	    }

	    public void Disable()
	    {
		    foreach (var c in 		    _charactersInside)
		    {
			    c.OnTriggerExit(_colliderTrigger) ;
		    }
		    
		    _charactersInside.Clear();
		    gameObject.SetActive(false);

			    
		    Core.Instance.Input.Unsubscribe(this);

	    }

	    private void Update()
		{
			var xWorld =  _cameraVectorX * _input.GetHorizontal();
			var zWorld = _cameraVectorZ * _input.GetVertical();

			_moveDir = (xWorld + zWorld) * _speed;// * Time.deltaTime;
			_moveDir.Normalize();
			
			if (IsMoving)
			{
				var newRot = Quaternion.LookRotation(_moveDir);
				_arrowRoot.rotation = Quaternion.Lerp(_arrowRoot.rotation, newRot, Time.deltaTime * _arrowRotSpeed);	
			}
		}

	    public void OnInputStarted()
	    {
		    foreach (var controller in _charactersInside)
		    {
			    controller.Fsm.SetState(new SoldierManualMoveState(controller));
		    }
	    }

	    public void OnInputFinished()
	    {
		    foreach (var controller in _charactersInside)
		    {
			    controller.Fsm.SetState(new SoldierIdleOnBattleState(controller));
		    }
		    
		    _rigidbody.velocity = Vector3.zero;
	    }

	    private void FixedUpdate()
	    {
		    _rigidbody.velocity = _moveDir*Speed;

		    if (IsMoving)
		    {
			    foreach (var c in _charactersInside)
			    {
				    var dir = transform.position - c.transform.position;
				    var p = dir.magnitude / _radius;
				    var force = _funnelForceByDistanceToCenter.Evaluate(p) * _maxFunnelForce;

				    c.Rigidbody.AddForce(dir.normalized*force, ForceMode.VelocityChange);
			    }
		    }
		    
	    }

	

	    public Vector3 GetPositionToMove()
	    {
		    /*var pos = _arrowLoc.position;
		    pos.y = 0f;
		    return pos;*/
		    
		    var inputX = _input.GetHorizontal();
		    var inputZ = _input.GetVertical();

		    var xWorld = _cameraVectorX * inputX;
		    var zWorld = _cameraVectorZ * inputZ;

		    //xWorld = Vector3.Project(zWorld, xWorld);
		    
		    var moveDir = (xWorld + zWorld) ;
		    
		    if (moveDir.magnitude > 1)
		    {
			    moveDir = moveDir.normalized ;
		    }
		    
			//moveDir.Normalize();
		   // var s = Mathf.Lerp(0, _radius, moveDir.magnitude/2f);

		    //moveDir = moveDir.normalized * s;

		    return transform.position + moveDir * Radius;
		    //    */

		    /*var res = transform.position + (_arrowLoc.position - transform.position)/2 ;
		    res.y = 0;
		    return res;*/

		    /*var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		    GameObject.Destroy(go.GetComponent<Collider>());
		    go.transform.localScale = Vector3.one * 0.2f;
		    go.transform.position = res;
                    
		    GameObject.Destroy(go, 5f);*/

	    }

	    private void OnTriggerEnter(Collider other)
	    {
		 
		    if (other.GetComponent<SoldierController>() == null && other.GetComponent<PlayerController>()== null)
		    {
			    return;
		    }
		    
		    _charactersInside.Add(other.GetComponent<CharacterControllerBase>());
		    ActualizeRadius();
	    }
	    
	    private void OnTriggerExit(Collider other)
	    {
		    if (other.GetComponent<SoldierController>() == null && other.GetComponent<PlayerController>()== null)
		    {
			    return;
		    }
		    
		    _charactersInside.Remove(other.GetComponent<CharacterControllerBase>());

		    ActualizeRadius();
		 
	    }

	    private void ActualizeRadius()
	    {
		    _circleRoot.DOKill();

		    _count.text = _charactersInside.Count.ToString();
		    
		    _radius = _radiusByCharactersCount.Evaluate(_charactersInside.Count);
		    _circleRoot.DOScale(Vector3.one * _radius, 0.2f);
		    _colliderPhysical.radius = _radius;
		    _colliderTrigger.radius = _radius;
		    
		    Core.Instance.Camera.SetSoldiersCount(_charactersInside.Count);
	    }

	    public void OnSoldierDied(SoldierController soldier)
	    {
		    _charactersInside.Remove(soldier);

		    ActualizeRadius();
	    }

	    private void OnDrawGizmos()
	    {
		    Gizmos.color = Color.yellow;
		    Gizmos.DrawSphere(GetPositionToMove(), 0.5f);
	    }
	    
	    /*public Vector3 GetPosForCameraLook()
		{
		return  transform.position + (GetPositionToMove() - transform.position) * 0.3f;
		}*/
    }
}

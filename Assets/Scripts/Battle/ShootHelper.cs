﻿using System.Linq;
using Characters;
using Characters.FSM;
using Characters.FSM.Soldier.Battle;
using Common.Random;
using UnityEngine;
using Utils;

namespace Battle
{
    public static class ShootHelper
    {
        public static SoldierShootState GetShootStateIfCanShoot(CharacterControllerBase soldier)
        {
            if (Core.Instance.Zombies.Count == 0) return null;

            var nearestZombie = Core.Instance.Zombies.Where(z => TargetIsValidForAimer(soldier, z))
                
                .OrderBy(z => z.ShootersToMe)
                .ThenBy(z => MoveUtils.SqrDistance(z.Position, soldier.Position))
                .Take(Core.Instance.Constants.RandomCountOfNearestZombiesForShoot)
                .RandomElementUsing(new DotNetRandom(soldier.GetHashCode()));

            var zombieToShootExists = nearestZombie != null;
            if (zombieToShootExists)
            {
                return new SoldierShootState(soldier, nearestZombie);
            }

            return null;
            
            
        }
        public static bool TargetIsValidForAimer(CharacterControllerBase aimer, ZombieController target)
        {
            if (aimer == null) return false;
            
            if (target.ShootersToMe > 4) return false;
            
            var distToNearest = Vector3.Distance(aimer.Position, target.Position);
            if (distToNearest > aimer.AttackDistance)
                return false;
            
            var anyCollision = Physics.Raycast(aimer.Position,
                target.Position-aimer.Position,
                distToNearest,
                Core.Instance.Constants.EnvironmentLayerMask);
            
            if (anyCollision)
            {
                return false;
            }


            return true;

        }
    }
}
﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Battle.Fxs
{
    public class BulletFx : MonoBehaviour
    {
        private float _duration;
        private Transform _target;
        private Vector3 _targetPos;

        private Vector3 _startPos;
        private float _timeStart;
        private float TimeSpent => Time.time - _timeStart;
        private float Progress01 => TimeSpent / _duration;
        public async UniTask<Vector3> Play(float duration, Transform target, Vector3 targetPos)
        {
            gameObject.SetActive(true);
            _duration = duration;
            _target = target;
            _targetPos = targetPos;

            _timeStart = Time.time;
            _startPos = transform.position;

            await UniTask.WaitWhile(()=>Progress01 < 1f);

            return transform.position;
        }

        private void Update()
        {
            if (_target != null)
            {
                _targetPos = _target.position +Vector3.one;
            }
            
            transform.position = Vector3.Lerp(_startPos, _targetPos, Progress01);

            if (Progress01 >= 1f)
            {
                Destroy(gameObject);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Characters;
using Characters.CharacterInput;
using Characters.FSM;
using Characters.FSM.Player.Base;
using Characters.FSM.Soldier;
using Characters.FSM.Soldier.Battle;
using Cysharp.Threading.Tasks.Triggers;
using Entities;
using UnityEngine;
using Utils;
using Object = UnityEngine.Object;

namespace Battle
{
    public class Core
    {
        private static Core _instance;
        public static Core Instance
        {
            get
            {
                if (_instance == null)
                {
                    throw new Exception("Try use Core not from Battle state");
                }

                return _instance;
            }
        }

        public readonly PlayerController Player;
        public readonly CoreCamera.CoreCamera Camera;
        public readonly ParadeLine ParadeLine;
        public readonly ControlZone ControlZone;
        public readonly CoreConstants Constants;

        public readonly List<SoldierController> Soldiers = new List<SoldierController>();
        public readonly List<ZombieController> Zombies = new List<ZombieController>();

        public readonly CharacterInputBase Input;
        public readonly BattleUI BattleUI;
        public CoreState State { get; private set; }
        public Core()
        {
            if (_instance != null)
            {
                throw new Exception("Duplicate core");
            }
            
            _instance = this;


            Constants =  Resources.Load<CoreConstants>("Prefabs/CoreConstants");

            BattleUI =  GameObject.Instantiate(Resources.Load<BattleUI>("Prefabs/UI/BattleUI"));

            Input = new JoystickInput(BattleUI.Joystick);

            Player = GameObject.Instantiate(Resources.Load<PlayerController>("Prefabs/Player"));
            Player.Initialize();
            
            Camera =  GameObject.Instantiate(Resources.Load<CoreCamera.CoreCamera>("Prefabs/Camera"));
            Camera.OnEnterBase();
           

            ParadeLine = GameObject.FindObjectOfType<ParadeLine>();

            ControlZone =  GameObject.Instantiate(Resources.Load<ControlZone>("Prefabs/ControlZone"));
            ControlZone.Disable();
            

            
            GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/UI/EventSystem"));

        }

        public void OnPlayerEnterBattleZone()
        {
            if (State != CoreState.Battle)
            {
                State = CoreState.Battle;

                ControlZone.Enable();

                Object.FindObjectsOfType<EntityBase>().ForEach(e=>e.OnPlayerEnterBattleZone());

                    //Zombies.ForEach(z =>z.OnPlayerEnterBattleZone());
                Soldiers.ForEach(e=>e.OnPlayerEnterBattleZone());
                Player.Fsm.SetState(SoldierHelper.CalculateNextState(Player));
                
                Camera.SetTargetTransform(ControlZone.transform);
            }
        }

        public void OnPlayerEnterBaseZone()
        {
            if (State != CoreState.Base)
            {
                State = CoreState.Base;
                
                Object.FindObjectsOfType<EntityBase>().ForEach(e=>e.OnPlayerEnterBaseZone());
                ControlZone.Disable();

                Player.Fsm.SetState(new PlayerRunOnBaseState(Player));
                Zombies.ForEach(z =>z.OnPlayerEnterBaseZone());
                Soldiers.ForEach(e=>e.OnPlayerEnterBaseZone());
                
                Camera.OnEnterBase();
            }
        }

        public void CharacterEnabled(CharacterControllerBase character)
        {
            if (character is ZombieController z)
            {
                Zombies.Add(z);
            }
            else  if (character is SoldierController s)
            {
                Soldiers.Add(s);
            }
        }
        
        public void CharacterDisabled(CharacterControllerBase character)
        {
            if (character is ZombieController z)
            {
                Zombies.Remove(z);
            }
            else  if (character is SoldierController s)
            {
                Soldiers.Remove(s);
            }
        }
    }
}
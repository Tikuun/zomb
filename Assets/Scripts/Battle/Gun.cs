using Battle.Fxs;
using Characters;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace Battle
{
   public class Gun : MonoBehaviour
   {
      [SerializeField] private ParticleSystem _onStart;
      [SerializeField] private BulletFx _bulletRes;
      [SerializeField] private ParticleSystem _onEnd;

      void Awake()
      {
         _bulletRes.gameObject.SetActive(false);
      }
      public void Shoot(float duration, Transform zombie, Vector3 zombiePos)
      {
         ShootAsync().Forget();         

         async UniTask ShootAsync()
         {
            _onStart.Play();

            var bulletInst =
               GameObject.Instantiate(_bulletRes, _bulletRes.transform.position, _bulletRes.transform.rotation);
            
            var endPos =await bulletInst.Play(duration, zombie, zombiePos);

            if (_onEnd != null)
            {
               _onEnd.transform.position = endPos;
               _onEnd.Play();
            }
            
         }
      }
   }
}

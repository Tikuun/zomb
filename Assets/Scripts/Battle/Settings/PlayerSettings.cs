﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Characters
{
    [Serializable]
    public class PlayerSettings
    {
        [Tooltip("Здоровье")] [SerializeField] public int PlayerMaxHealth = 100;
        [Tooltip("Зона видимости стрельбы")][SerializeField] public float PlayerViewDistance = 10f;

        [Tooltip("Скорость бега по базе")] [SerializeField] public float PlayerOnBase_MoveSpeed = 10f;
        [Tooltip("Скорость разворота при беге по базе")] [SerializeField] public float PlayerOnBase_TurnSpeed = 10f;
        
        [Tooltip("Масса при беге (везде)")] [SerializeField] public float Mass_Run = 10f;
        [Tooltip("Масса при Idle/Shoot/etc (везде)")] [SerializeField] public float Mass_Idle = 10f;
        [Tooltip("Урон за 1 удар по ящику с досками")] [SerializeField]public int HitDamageForeResourceBox=20;
        [Tooltip("Длительность анимации удара по ящику до удара")] [SerializeField]public float OpenResBox_BeforeHitTime = 0.5f;
        [Tooltip("Длительность анимации удара по ящику после удара")] [SerializeField]public float OpenResBox_AfterHitTime = 1f;
       
    }
}
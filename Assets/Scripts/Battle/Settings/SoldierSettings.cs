﻿using System;
using UnityEngine;

namespace Characters
{
    [Serializable]
    public class SoldierSettings
    {
        [Tooltip("Здоровье")] [SerializeField] private int _healthMax = 100;
        [Tooltip("Зона видимости стрельбы" )] [SerializeField] private float _viewDistance = 10f;

        [Tooltip("Погрешность нахождения в точке построения")] [SerializeField] private float _distanceToParadelinePoint = 0.25f;
        
        //ПЛАЦ
        [Tooltip("Скорость бега по базе (до плаца)")] [SerializeField] private float _moveSpeedToParadeline = 10f;
        [Tooltip("Скорость разворота при беге до плаца")] [SerializeField] private float _turnSpeedToParadeline = 10f;
        [Tooltip("Скорость разворота на плацу")] [SerializeField] private float _turnSpeedOnParadeline = 10f;
        
        [Tooltip("Масса при беге")] [SerializeField] public float Mass_Run = 10f;
        [Tooltip("Масса при Idle/Shot/etc")] [SerializeField] public float Mass_Idle = 10f;
        public int Health => _healthMax;
        public float ViewDistance => _viewDistance;
        public float DistanceToParadelinePoint => _distanceToParadelinePoint;
        public float MoveSpeedToParadeline => _moveSpeedToParadeline;
        public float TurnSpeedToParadeline => _turnSpeedToParadeline;
        public float TurnSpeedOnParadeline => _turnSpeedOnParadeline;
      
    }
}
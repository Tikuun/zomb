﻿using System.ComponentModel;
using Battle.Settings;
using Characters;
using UnityEngine;
using UnityEngine.Serialization;

namespace Battle
{
     [CreateAssetMenu(fileName = "CoreConstants", menuName = "Configs/CoreConstants")]
    public class CoreConstants : ScriptableObject
    {
        [Header("Настройки Игрока ")] 

        [SerializeField] public PlayerSettings PlayerSetting ;


        [Header("Общие настройки солдат и игрока в бою ")] 
        
        [Tooltip("График множителя скорости в зависимости от удаленности от точки-цели на периметре круга управления")] 
        [SerializeField] public AnimationCurve SpeedInsideCZCurve;
        
        [Tooltip("Скорость бега к кругу по прямой")] [SerializeField] public float WhenSeeCZ_MoveSpeed = 10f;
        [Tooltip("Скорость разворота при беге к кругу по прямой")] [SerializeField] public float WhenSeeCZ_TurnSpeed = 10f;
        
        [Tooltip("Скорость разворота при беге внутри круга")] [SerializeField] public float ManualMove_TurnSpeed = 10f;
        [Tooltip("Скорость бега внутри круга")] [SerializeField] public float ManualMove_MoveSpeed = 10f;

        [Tooltip("Скорость бега к кругу если нет прямого пути")] [SerializeField] public float WhenDontSeeCZ_MoveSpeed = 10f;
        [Tooltip("Скорость разворота при беге к кругу если нет прямого пути")] [SerializeField] public float WhenDontSeeCZ_TurnSpeed = 10f;

        [Tooltip("Рандомно Выбираем из N ближайших зомби для выстрела")] [SerializeField] public int RandomCountOfNearestZombiesForShoot = 10;
        [Tooltip("Урон")][SerializeField] public int HitDamage = 50;
        [Tooltip("Скорость пули(не визуал)")] [SerializeField] public float HitSpeed = 20f;
        [Tooltip("Сила толчка зомби от пули")] [SerializeField]public float HitPushForce = 10f;
        [SerializeField] [Range(0f, 1f)]public float HitProbability = 1f;

        
        [Tooltip("Мин рандомная подготовка перед выстрелом")] [SerializeField] public float MinPrepareShotTime = 0f;
        [Tooltip("Макс рандомная подготовка перед выстрелом")] [SerializeField] public float MaxPrepareShotTime = 1f;
        [Tooltip("В анимации выстрела время от старта анимации до вылета пули")] [SerializeField] public float TimeOfShotFromAnimStart = 0.2f;


        [Tooltip("Скорость поворота на цель в стейте 'стреляю'")] [SerializeField] public float WhenShoot_TurnSpeed = 10f;

        [FormerlySerializedAs("BattleRunSettings")] [SerializeField] public BattleRunSettings SoldiersRunSettings;

        [Header("Настройки Солдат ")] 
        [SerializeField] public SoldierSettings SoldierSettings;

        [Header("Настройки Зомби ")] 
        [SerializeField] public ZombieSettings ZombieSettings;
        
        [Header("Слои")]
        [SerializeField] public LayerMask EnvironmentLayerMask;

        public float DistanceInCZConsiderStuck = 0.5f;
    }
}
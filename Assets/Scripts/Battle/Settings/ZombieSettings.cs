﻿using System;
using Battle.Settings;
using UnityEngine;
using UnityEngine.Serialization;

namespace Characters
{
    [Serializable]
    public class ZombieSettings
    {
        [Tooltip("Здоровье")] [SerializeField] private int _healthMax = 100;
        [Tooltip("Зона видимости")] [SerializeField] private float _viewDistance = 10f;

        [Tooltip("Расстояние для кусания")] [SerializeField]  public float AttackDistance = 3f;


        [Tooltip("Нет цели - тупим - мин время")] [SerializeField] private float _minPauseWhenNoTarget = 1f;
        [Tooltip("Нет цели - тупим - макс время")] [SerializeField] private float _maxPauseWhenNoTarget = 3f;

        //no target -> return to spawn
        [Tooltip("Скорость возвращения на спавн п")] [SerializeField] private float _moveSpeedWhenReturnToSpawn = 3f;
        [Tooltip("Скорость разворота при возвращении на спавн п")] [SerializeField] private float _turnSpeedWhenReturnToSpawn = 3f;
        
        //when see target
        [Tooltip("Скорость когда вижу цель")] [SerializeField] private float _moveSpeedWhenSeeTarget = 10f;
        [Tooltip("Скорость разворота когда вижу цель")] [SerializeField] private float _turnSpeedWhenSeeTarget = 10f;

        [Tooltip("Урон")] [SerializeField] private int _hitDamage = 30;
        [Tooltip("Масса при Idle/Bite/etc")] [SerializeField] public float Mass_Idle = 10;
        [Tooltip("Масса при беге")] [SerializeField] public float Mass_Run = 5;
        
        [Tooltip("Мин время на подготовку укуса")] [SerializeField] public float MinPrepareBiteTime = 0f;
        [Tooltip("Макс время на подготовку укуса")] [SerializeField] public float MaxPrepareBiteTime = 1f;


        [SerializeField] public BattleRunSettings RunSettings;

        public int HealthMax => _healthMax;
        public float ViewDistance => _viewDistance;

        public float MoveSpeedToSpawnPoint => _moveSpeedWhenReturnToSpawn;
        public float TurnSpeedToSpawnPoint => _turnSpeedWhenReturnToSpawn;
        public float MinPauseWhenNoTarget => _minPauseWhenNoTarget;
        public float MaxPauseWhenNoTarget => _maxPauseWhenNoTarget;
        public float MoveSpeedWhenSeeTarget => _moveSpeedWhenSeeTarget;
        public float TurnSpeedWhenSeeTarget => _turnSpeedWhenSeeTarget;
        public int HitDamage => _hitDamage;
    }
}
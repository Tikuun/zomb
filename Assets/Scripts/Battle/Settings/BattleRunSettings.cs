﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Battle.Settings
{
    [Serializable]
    public class BattleRunSettings
    {
        [Tooltip("Кривая Wiggle")] [SerializeField] public AnimationCurve runSpeedWiggleCurve01;
        [Tooltip("Длина волны Wiggle")] [SerializeField] public float RunWiggleDuration;

        [Tooltip("Анимация создана под скорость перемещения N")] [SerializeField]
        public float DefaultSpeedForRunAnimation = 5f;

    }
}
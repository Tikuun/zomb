﻿using Characters;
using UnityEngine;

namespace Entities
{
    public class CharacterSpawner : EntityBase
    {
        [SerializeField] private int _count;
        [SerializeField] private CharacterControllerBase _generatesCharacter;
        [SerializeField] private float _cooldown = 1f;
        [SerializeField] private Transform _spawnLocator;

        private bool _activated;
        private float _timeLastSpawn;

        private bool IsCooldown => Time.time < _timeLastSpawn + _cooldown;

        protected override void OnTriggerEnter(Collider other)
        {
            if (_activated) return;
            
            var character = other.GetComponent<CharacterControllerBase>();
            if (character == null || !character.CanActivateCharacterSpawner())
            {
                return;
            }

            _activated = true;
        }

    
        private void FixedUpdate()
        {
            if (!_activated) return;
            if (_count == 0) return;
            if (IsCooldown) return;

            Spawn();
        }

        private void Spawn()
        {
            _count--;
            _timeLastSpawn = Time.time;

            if (_count == 0)
            {
                Destroy(gameObject);
            }

            var xRand = Random.Range(-1f, 1f);
            var xPos = _spawnLocator.transform.position.x + xRand * _spawnLocator.transform.localScale.x / 2f;
            
            var zRand = Random.Range(-1f, 1f);
            var zPos = _spawnLocator.transform.position.z + zRand * _spawnLocator.transform.localScale.z / 2f;

            var pos = new Vector3(xPos, 0, zPos);
            
            var character = Instantiate(_generatesCharacter, pos, Quaternion.identity);
            character.Initialize();
        }
        
        private void OnDrawGizmos()
        {
            var c = Color.red;
            c.a = 0.2f;
            Gizmos.color = c;
            Gizmos.DrawCube(transform.position, transform.localScale);
            
            c.a = 0.5f;
            Gizmos.color = c;
            Gizmos.DrawCube(_spawnLocator.position, _spawnLocator.localScale);
        }
    }
}
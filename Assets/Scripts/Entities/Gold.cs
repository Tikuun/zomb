﻿using Characters;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Entities
{
    public class Gold : EntityBase
    {
        protected override void OnTriggerEnter(Collider other)
        {
            var character = other.GetComponent<PlayerController>();
            if (character != null)
            {
                 Collect(character).Forget();
            }
        }

        public async UniTask Collect(PlayerController character)
        {
            var go = gameObject;
            
            Disable();
            Destroy(this);

            await character.Stack.CollectGold(gameObject);
            Destroy(go);
        }
    }
}
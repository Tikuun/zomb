﻿using System;
using System.Collections.Generic;
using System.Linq;
using Characters;
using UnityEngine;

namespace Entities
{
    public class ParadeLine : EntityBase
    {
        [SerializeField] private List<ParadeLineLocator> _slots;

        public ParadeLineLocator BookFreeSlot(SoldierController soldier)
        {
            var freeSlot = GetFreeSlot();
            if (freeSlot != null)
            {
                freeSlot.Book(soldier);
            }

            return freeSlot;
        }


        public bool HasFreeSlot()
        {
            return GetFreeSlot() != null;
        }

        private ParadeLineLocator GetFreeSlot()
        {
            var freeSlot = _slots.FirstOrDefault(s => !s.IsBusy);
            return freeSlot;
        }

        private void OnValidate()
        {
            _slots = transform.GetComponentsInChildren<ParadeLineLocator>().ToList();
        }

      
    }
}
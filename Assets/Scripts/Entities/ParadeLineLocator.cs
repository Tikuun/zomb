﻿using Characters;
using UnityEngine;

namespace Entities
{
    public class ParadeLineLocator : MonoBehaviour
    {
        [SerializeField] private CharacterControllerBase _controller;

        public bool IsBusy => _controller != null;

        public void Book(SoldierController soldier)
        {
            _controller = soldier;
        }
        
        public void UnBook()
        {
            _controller = null;
        }
    }
}
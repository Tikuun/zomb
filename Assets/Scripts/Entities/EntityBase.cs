﻿using UnityEngine;

namespace Entities
{
    public abstract class EntityBase : MonoBehaviour
    {
        //todo [SerializeField] private LayerMask _layersInteractable;
         [SerializeField] private Collider _collider;
        
        protected virtual void OnTriggerEnter(Collider other){}
        protected virtual void OnTriggerExit(Collider other){}
        protected virtual void OnTriggerStay(Collider other){}

        public void Enable()
        {
            _collider.enabled = true;
        }

        public void Disable()
        {
            _collider.enabled = false;
        }

        public virtual void OnPlayerEnterBattleZone()  {  }
        public virtual void OnPlayerEnterBaseZone()  {   }
    }
}
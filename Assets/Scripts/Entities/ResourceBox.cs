﻿using Battle;
using Characters;
using Characters.FSM.Player.Base;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Entities
{
    public class ResourceBox : EntityBase
    {
        [SerializeField] private Image _healthBar;

        [SerializeField] private int _healthMax = 100;
        
        [SerializeField] private int _reward;
        [SerializeField] private Animator _animator;
        [SerializeField] private GameObject _destroyFx;
        
        private int _health;

        private void Awake()
        {
            _health = _healthMax;
            ActualizeHealthBar();
        }

        protected override void OnTriggerStay(Collider other)
        {
            if (!IsAlive())
            {
                return;
            }
            
            var character = other.GetComponent<CharacterControllerBase>();
            if (character!= null && character.Fsm.CurrentState.CanSwitchToOpenResourceBox())
            {
                character.Fsm.SetState(new PlayerOpenResourceBoxState(character, this));
            }
        }

        public void OnHit(int damage)
        {
            _animator.SetTrigger("Hit");
            _health -= damage;
            if (_health < 0) _health = 0;
            
            ActualizeHealthBar();

            if (!IsAlive())
            {
                DestroyMe();
            }
        }

        private void DestroyMe()
        {
            var fx = GameObject.Instantiate(_destroyFx, transform.position, Quaternion.identity);
           Destroy(fx, 5f); 
            
            Disable();
            Destroy(gameObject);

            var rootPos = gameObject.transform.position;
            var res = Resources.Load<Gold>("Prefabs/Gold");
            for (int i = 0; i < _reward; i++)
            {
                 CreateReward(i, _reward).Forget();

            }


            async UniTask CreateReward(int index, int maxCount)
            {
                var randRotation = Quaternion.Euler(0, Random.Range(0f, 360f), 0f);
                
                var gold = Instantiate(res, rootPos, randRotation);
                gold.Disable();

                var angle = 360f / maxCount * index + 1;
                var targetRot = Quaternion.AngleAxis(angle, Vector3.up);
                
                var dir = targetRot * Vector3.forward;

                var newPos = rootPos + dir.normalized * Random.Range(1.5f,3f);

                var middlePoint = gold.transform.position + (newPos - rootPos) / 2 + Vector3.up*0.5f;
                    
                Sequence mySequence = DOTween.Sequence();
                mySequence.Append(gold.transform.DOMove(middlePoint, 0.5f));
                mySequence.Append(gold.transform.DOMove(newPos, 0.5f));
                mySequence.SetEase(Ease.OutCirc);
                await mySequence.Play();
                

                //gold.Collect(Core.Instance.Player).Forget();
                gold.Enable();
            }
        }

        private void ActualizeHealthBar()
        {
            _healthBar.fillAmount = _health / (float)_healthMax;
        }

        public bool IsAlive()
        {
            return _health > 0;
        }
    }
}
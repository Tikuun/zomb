﻿using Battle;
using Characters;
using UnityEngine;

namespace Entities
{
    public class BaseZoneTrigger : EntityBase
    {
        [SerializeField] private Color _color;
        /*
        protected override void OnTriggerEnter(Collider other)
        {
            var player = other.GetComponent<PlayerController>();
            if (player != null)
            {
                Core.Instance.OnEnterBaseZone();
            }
        }
        
        protected override void OnTriggerExit(Collider other)
        {
            var player = other.GetComponent<PlayerController>();
            if (player != null)
            {
                Core.Instance.OnEnterBattleZone();
            }
        }
        */
        
        private void OnDrawGizmos()
        {
            Gizmos.color = _color;
            Gizmos.DrawCube(transform.position, transform.localScale);
        }

    }
}
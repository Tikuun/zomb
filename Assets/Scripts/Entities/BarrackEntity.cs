﻿using Battle;
using Characters;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Entities
{
    public class BarrackEntity : EntityBase
    {
        [SerializeField] private int _upgradePrice = 10;
        [SerializeField] private TextMeshProUGUI _remainPrice;
        
        [SerializeField] private Transform _spawnLocator;
        [SerializeField] private CharacterControllerBase _generatesCharacter;
        [SerializeField] private float _generateDuration = 5f;
        
        [SerializeField] private BarrackEntity _nextLevelBarrack;

        private float _generateProgress ;
        
        private int _alreadySpent;
        
        private int RemainPrice => _upgradePrice - _alreadySpent;

        private bool HasNextLevel => _nextLevelBarrack != null;
        private void Awake()
        {
            _remainPrice.text = RemainPrice.ToString();
            if (!HasNextLevel)
            {
                _remainPrice.gameObject.SetActive(false);
            }
            
            DelayedEnable().Forget();
            
            async UniTask DelayedEnable()
            {
                Disable();
                await UniTask.Delay(2000);
                Enable();
            }
        }

        protected override void OnTriggerStay(Collider other)
        {
            if (!HasNextLevel) return;
            
            var player = other.GetComponent<PlayerController>();
            if (player == null) return;

            TrySpendGoldFromPlayer(player);
        }

        private void TrySpendGoldFromPlayer(PlayerController player)
        {
            if (!player.CanSpendGold()) return;

            Spend().Forget();
            
            async UniTask Spend()
            {
                await player.Stack.SpendGold(transform.position);
                _alreadySpent++;

                _remainPrice.text = RemainPrice.ToString();

                if (RemainPrice == 0)
                {
                    Destroy(gameObject);
                    Instantiate(_nextLevelBarrack, transform.position, transform.rotation);
                }
            }
        }

        private void FixedUpdate()
        {
            if (Core.Instance.State != CoreState.Base) return;
            if (_generatesCharacter == null) return;
            if (!Core.Instance.ParadeLine.HasFreeSlot()) return;
            
            _generateProgress += Time.fixedDeltaTime;

            
            if (_generateProgress >= _generateDuration)
            {
                _generateProgress = 0;
                var soldier = Instantiate(_generatesCharacter, _spawnLocator.position, Quaternion.identity);
                soldier.Initialize();
                
            }
        }
    }
}